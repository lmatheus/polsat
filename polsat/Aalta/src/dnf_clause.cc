/*
 * File:   dnf_clause.cc
 * Author: snowingsea
 * 
 * Created on July 20, 2013, 10:53 AM
 */

#include "dnf_clause.hh"
#include "utility.hh"


  namespace aalta
  {

    dnf_clause::dnf_clause (aalta_formula* current, aalta_formula* next)
    {
      this->current = current;
      this->next = next;
      this->clc_hash ();
    }

    dnf_clause::dnf_clause (const dnf_clause& orig)
    {
      *this = orig;
    }

    /**
     * 重载赋值操作符
     * @param dc
     * @return 
     */
    dnf_clause& dnf_clause::operator = (const dnf_clause& dc)
    {
      if (this != &dc)
        {
          this->current = dc.current;
          this->next = dc.next;
          this->hash = dc.hash;
        }
      return *this;
    }

    /**
     * 重载等于符号
     * @param dc
     * @return 
     */
    bool dnf_clause::operator == (const dnf_clause& dc) const
    {
      return this->current == dc.current && this->next == dc.next;
    }

    /**
     * 重载小于号，stl_map中用
     * @param dc
     * @return 
     */
    bool dnf_clause::operator< (const dnf_clause& dc) const
    {
      return this->next->get_length () < dc.next->get_length ();
      if (this->next->get_length () != dc.next->get_length ())
        return this->next->get_length () < dc.next->get_length ();
      if (this->current == dc.current)
        return this->next < dc.next;
      return this->current < dc.current;
    }

    /**
     * To String
     * @return 
     */
    std::string
    dnf_clause::to_string () const
    {
      return "[ " + this->current->to_string () +
              " -> " + this->next->to_string () + " ]";
    }

    /**
     * 计算hash值
     */
    void
    dnf_clause::clc_hash ()
    {
      hash = 1315423911;
      hash = (hash << 5) ^ (hash >> 27) ^ (size_t) current;
      hash = (hash << 5) ^ (hash >> 27) ^ (size_t) next;
    }
  }
