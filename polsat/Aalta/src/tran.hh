/* 
 * File:   tran.hh
 * Author: Jianwen Li
 *
 * tran = <current, next> 
 * 
 * Created on Aug 26th, 2013
 */

#ifndef TRAN_HH
#define	TRAN_HH

#include "aalta_formula.hh"
#include "state.hh"


  namespace aalta
  {
    class state
    {
    public:
      //constructor
      tran(aalta_formula *curr, state *n)
      {
        current = curr;
        next = n;
      }
      //destroyer
      void destroy()
      {
        if(current != NULL)
          current->destroy();
        if(next != NULL)
          next->destroy();
        delete this;
      }
      
      std::string to_string()
      {
        return to_promela();
      }
      std::string to_promela()
      {
        std::string result = "";
        result += current->to_string() + " -> " + next->get_promela_name() + ";\n"
        return result;
      }
    private:
      aalta_formula *current;
      state* next;
    };
  }

#endif	/* TRAN_HH */

