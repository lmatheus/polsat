/* 
 * File:   olg_formula.cc
 * Author: snowingsea
 * 
 * Created on July 31, 2013, 6:17 PM
 */

#include <limits.h>
#include <algorithm>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>
using namespace std;

#include "olg_formula.hh"
#include "utility.hh"
#include "printer.hh"

  namespace aalta
  {

    bool
    olg_formula::sat (const std::string& f_str)
    {
      if (f_str == "True")
        return true;
      if (f_str == "False")
        return false;

      file_write ("tmp.in", f_str.c_str ());
      char cwd[256];
      if (getcwd(cwd, sizeof(cwd)) == NULL)
      {
        Printer::printErrorMessage("getcwd() error");
        exit(0);
      }
      string bool2cnf(cwd);
      string minisat(cwd);
      std::size_t found = bool2cnf.find("polsat/Aalta");
      if (found != std::string::npos)
      {
        bool2cnf += "/../bool2cnf/a.out < tmp.in > cnf.tmp.out";
        minisat += "/../minisat/simp/minisat cnf.tmp.out sat.ans >/dev/null 2>&1";
      }
      else
      {
        bool2cnf += "/../Aalta/bool2cnf/a.out < tmp.in > cnf.tmp.out";
        minisat += "/../Aalta/minisat/simp/minisat cnf.tmp.out sat.ans >/dev/null 2>&1";
      }
      system (bool2cnf.c_str());    
      system (minisat.c_str());
      FILE* fans = fopen ("sat.ans", "r");
      char ans[10];
      fgets (ans, 10, fans);
      fclose (fans);
      remove ("tmp.in");
      remove ("cnf.tmp.out");
      remove ("sat.ans");
      return ans[0] == 'S';
    }

    olg_formula::olg_formula (aalta_formula* af)
    {
      this->id_arr = this->pos_arr = NULL;
      this->id_size = this->pos_size = 0;

      aalta_formula* next = af;
      while (next != NULL)
        {
          this->split (next->af_now (aalta_formula::And));
          next = next->af_next (aalta_formula::And);
        }
      std::list<Item*>::const_iterator lit;
      this->root = NULL;
      for (lit = this->G.begin (); lit != this->G.end (); lit++)
        {
          if (this->root == NULL)
            this->root = *lit;
          else
            this->root = new Item (aalta_formula::And, *lit, this->root);
        }
      for (lit = this->NGF.begin (); lit != this->NGF.end (); lit++)
        {
          if (this->root == NULL)
            this->root = *lit;
          else
            this->root = new Item (aalta_formula::And, *lit, this->root);
        }
    }

    olg_formula::~olg_formula ()
    {
      this->destroy (this->root);
      if (this->pos_arr != NULL)
        {
          delete[] this->pos_arr;
          this->pos_arr = NULL;
        }
      if (this->id_arr != NULL)
        {
          delete[] this->id_arr;
          this->id_arr = NULL;
        }

      for (std::list<Item*>::const_iterator it = this->GF.begin (); it != this->GF.end (); it++)
        this->destroy (*it);
    }

    bool
    olg_formula::sat ()
    {
      //TODO: 内存泄漏 ngf
      if (sat (this->to_olg_string ())) return true;
      if (this->GF.empty ()) return false;

      std::list<Item*>::const_iterator lit;
      Item* ngf = NULL;
      for (lit = this->NGF.begin (); lit != this->NGF.end (); lit++)
        {
          if (!(*lit)->is_more ())
            return false;
          if (ngf == NULL)
            ngf = *lit;
          else
            ngf = new Item (aalta_formula::And, *lit, ngf);
        }
      bool s = true;
      //      if(ngf != NULL)Printer::printMessage ("NGF: " + ngf->to_olg_string ());
      for (lit = this->GF.begin (); lit != this->GF.end () && s; lit++)
        {
          //          Printer::printMessage ("GF: " + (*lit)->to_olg_string ());
          if (ngf == NULL)
            {
              s &= sat ((*lit)->to_olg_string ());
              //            Printer::printMessage ((*lit)->to_olg_string () + "\t" + (s?"True":"False"));
            }
          else
            {
              s &= sat ((*lit)->to_olg_string () + " & " + ngf->to_olg_string ());

              //            Printer::printMessage ((*lit)->to_olg_string () + " & " + ngf->to_olg_string () + "\t" + (s?"True":"False"));
            }
        }
      //      this->destroy (ngf);
      return s;
    }

    bool
    olg_formula::unsat ()
    {
      //      Printer::printMessage ("olg: " + root->to_string ());
      std::string str = root->to_olg_more_string (INT_MAX);
      //      Printer::printMessage ("more: " + str);
      if (!(str.empty () || sat (str)))
        {
          return true;
        }

      std::string gstr;
      for (std::list<Item*>::const_iterator lit = this->ALLG.begin (); lit != this->ALLG.end (); lit++)
        {
          gstr = (*lit)->to_olg_string ();
          if (!sat (gstr))
            continue;
          if (!sat (str + " & " + gstr))
            return true;
        }

      if (this->pos_arr == NULL)
        {
          this->pos_arr = new int[pos_size + 10];
          if (this->id_arr == NULL)
            this->id_arr = new int[pos_size + 10];
          this->pos_size = this->id_size = 0;
          this->col_info (this->root);
        }
      if (pos_size > 0)
        {
          std::sort (pos_arr, pos_arr + pos_size);
          int i = 0;
          str = root->to_olg_more_string (pos_arr[0]);
          //          Printer::printMessage ("0: " + str);
          if (!(str.empty () || sat (str)))
            return true;
          for (int k = 1; k < pos_size; ++k)
            {
              if (pos_arr[k] != pos_arr[i])
                {
                  str = root->to_olg_more_string (pos_arr[k]);
                  //                  Printer::printMessage (convertToString(i+1) + ": " + str);
                  if (!(str.empty () || sat (str)))
                    return true;
                  pos_arr[++i] = pos_arr[k];
                }
            }
        }

      if (id_size > 0)
        {
          std::sort (id_arr, id_arr + id_size);
          int i = 0;
          str = root->to_olg_id_string (id_arr[0]);
          if (!(str.empty () || sat (str)))
            return true;
          for (int k = 1; k < id_size; ++k)
            {
              if (id_arr[k] != id_arr[i])
                {
                  str = root->to_olg_id_string (id_arr[k]);
                  if (!(str.empty () || sat (str)))
                    return true;
                  id_arr[++i] = id_arr[k];
                }
            }
        }
      return false;
    }

    void
    olg_formula::split (aalta_formula* af)
    {
      //      Printer::printMessage (af->to_string ());
      if (af == NULL || af->oper () != aalta_formula::Release || af->l_af ()->oper () != aalta_formula::False)
        {
          this->NGF.push_back (build (af));
          return;
        }

      this->ALLG.push_back (build (af));

      aalta_formula* next = af->r_af ();
      aalta_formula* now;
      bool F = false;
      while (next != NULL)
        {
          now = next->af_now (aalta_formula::Or);
          if (now->oper () == aalta_formula::Until && now->l_af ()->oper () == aalta_formula::True)
            {
              now = now->r_af ();
              if (!F) this->GF.push_back (build (now));
              F = true;
            }
          if (!now->release_free ())
            {
              this->NGF.push_back (build (af));
              return;
            }
          next = next->af_next (aalta_formula::Or);
        }
      if (F) this->G.push_back (build (af));
      else this->NGF.push_back (build (af));
    }

    /* added by Jianwen Li
       Sept. 19th 2013
     */
    olg_formula::Item*
    olg_formula::build (aalta_formula* af)
    {
      pos_size++;
      Item* root = NULL;
      Item *left, *right;
      switch (af -> oper ())
        {
        case aalta_formula::And:
          left = build (af->l_af ());
          if (left->op == aalta_formula::False)
            {
              root = left;
            }
          else if (left->op == aalta_formula::True)
            {
              root = build (af->r_af ());
              this->destroy (left);
            }
          else
            {
              right = build (af->r_af ());
              if (right->op == aalta_formula::False)
                {
                  root = right;
                  this->destroy (left);
                }
              else if (right->op == aalta_formula::True)
                {
                  root = left;
                  this->destroy (right);
                }
              else
                {
                  root = new Item (aalta_formula::And, left, right);
                  if ((left -> compos == right -> compos) && (left -> compos >= 0))
                    {
                      root -> compos = left -> compos;
                    }
                  else
                    {
                      root -> compos = -1;
                    }
                }
            }
          break;
        case aalta_formula::Or:
          left = build (af->l_af ());
          if (left->op == aalta_formula::True)
            {
              root = left;
            }
          else if (left->op == aalta_formula::False)
            {
              root = build (af->r_af ());
              this->destroy (left);
            }
          else
            {
              right = build (af->r_af ());
              if (right->op == aalta_formula::True)
                {
                  root = right;
                  this->destroy (left);
                }
              else if (right->op == aalta_formula::False)
                {
                  root = left;
                  this->destroy (right);
                }
              else
                {
                  if ((left -> compos == right -> compos) && (left -> compos >= 0))
                    {
                      root = new Item (aalta_formula::Or, left, right);
                      root -> compos = left -> compos;
                    }
                  else
                    {
                      add_pos (left, aalta_formula::Until);
                      add_pos (right, aalta_formula::Until);
                      root = new Item (aalta_formula::Or, left, right);
                      root -> compos = -1;
                    }
                }
            }
          break;
        case aalta_formula::Next:
          root = build (af -> r_af ());
          add_pos (root, aalta_formula::Next);
          if (root -> compos >= 0)
            {
              root -> compos += 1;
            }
          break;
        case aalta_formula::Release:
          root = build (af -> r_af ());
          if (af -> l_af () -> oper () == aalta_formula::False)
            {
              add_pos (root, aalta_formula::Global);
            }
          break;
        case aalta_formula::Until:
          root = build (af->r_af ());
          add_pos (root, aalta_formula::Until);
          root -> compos = -1;
          break;
        case aalta_formula::True:
        case aalta_formula::False:
          root = new Item ((aalta_formula::opkind)af->oper ());
          root -> compos = 0;
          break;
        case aalta_formula::Not:
          root = new Item (aalta_formula::Not);
          root->atom = new Atom (af->r_af ()->oper (), 0, Once);
          root -> compos = 0;
          break;
        default:
          root = new Item (aalta_formula::Literal);
          root->atom = new Atom (af->oper (), 0, Once);
          root -> compos = 0;
          break;
        }
      return root;
    }

    void
    olg_formula::add_pos (Item* root, aalta_formula::opkind type)
    {
      if (root == NULL) return;
      Atom* atom = root -> atom;
      if (atom == NULL)
        {
          add_pos (root->left, type);
          add_pos (root->right, type);
          return;
        }

      switch (type)
        {
        case aalta_formula::Next:
          if (atom->pos >= 0) atom->pos++;
          break;
        case aalta_formula::Until:
          atom->pos = -1;
          break;
        case aalta_formula::Global:
          if (atom->freq == Once)
            {
              if (atom->pos < 0)
                atom->freq = Inf;
              else
                atom->freq = More;
            }
          break;
        }
    }

    /* commented by Jianwen Li
    olg_formula::Item*
    olg_formula::build (aalta_formula* af)
    {
      pos_size++;
      Item* root = NULL;
      Item *left, *right;
      if (af->oper () == aalta_formula::Release && af->l_af ()->oper () == aalta_formula::False)
        this->oplist.push_front (aalta_formula::Global);
      else
        this->oplist.push_front (af->oper ());
      switch (af->oper ())
        {
        case aalta_formula::And:
          left = build (af->l_af ());
          if (left->op == aalta_formula::False)
            {
              root = left;
            }
          else if (left->op == aalta_formula::True)
            {
              root = build (af->r_af ());
              this->destroy (left);
            }
          else
            {
              right = build (af->r_af ());
              if (right->op == aalta_formula::False)
                {
                  root = right;
                  this->destroy (left);
                }
              else if (right->op == aalta_formula::True)
                {
                  root = left;
                  this->destroy (right);
                }
              else
                {
                  root = new Item (aalta_formula::And, left, right);
                }
            }
          break;
        case aalta_formula::Or:
          left = build (af->l_af ());
          if (left->op == aalta_formula::True)
            {
              root = left;
            }
          else if (left->op == aalta_formula::False)
            {
              root = build (af->r_af ());
              this->destroy (left);
            }
          else
            {
              right = build (af->r_af ());
              if (right->op == aalta_formula::True)
                {
                  root = right;
                  this->destroy (left);
                }
              else if (right->op == aalta_formula::False)
                {
                  root = left;
                  this->destroy (right);
                }
              else
                {
                  root = new Item (aalta_formula::Or, left, right);
                }
            }
          break;
        case aalta_formula::Next:
        case aalta_formula::Release:
        case aalta_formula::Until:
          root = build (af->r_af ());
          break;
        case aalta_formula::True:
        case aalta_formula::False:
          root = new Item ((aalta_formula::opkind)af->oper ());
          break;
        case aalta_formula::Not:
          root = new Item (aalta_formula::Not);
          root->atom = new Atom (af->r_af ()->oper (), 0, Once);
          break;
        default:
          root = new Item (aalta_formula::Literal);
          root->atom = new Atom (af->oper (), 0, Once);
          break;
        }
      add_pos (root->atom);
      this->oplist.pop_front ();
      return root;
    }

    void
    olg_formula::add_pos (Atom* atom)
    {
      if (atom == NULL || atom->pos != 0)return;
      atom->pos = 1;
      std::list<int>::iterator it;
      for (it = oplist.begin (); it != oplist.end (); it++)
        {
          switch (*it)
            {
            case aalta_formula::Next:
              if (atom->pos >= 0) atom->pos++;
              break;
            case aalta_formula::Until:
              atom->pos = -1;
              break;
            case aalta_formula::Global:
              if (atom->freq == Once)
                {
                  if (atom->pos < 0)
                    atom->freq = Inf;
                  else
                    atom->freq = More;
                }
              break;
            }
        }
    }
     */

    void
    olg_formula::col_info (Item* root)
    {
      if (root == NULL)return;
      if (root->atom != NULL)
        {
          if (root->atom->pos > 0)
            this->pos_arr[this->pos_size++] = root->atom->pos;
          else if (root->op == aalta_formula::Not)
            this->id_arr[this->id_size++] = -root->atom->id;
          else
            this->id_arr[this->id_size++] = root->atom->id;
        }
      col_info (root->left);
      col_info (root->right);
    }

    int
    olg_formula::get_more_max_pos () const { }

    std::string
    olg_formula::to_olg_string () const
    {
      if (root == NULL)return "";
      return root->to_olg_string ();
    }

    std::string
    olg_formula::Atom::to_olg_string () const
    {
      return aalta_formula::get_name (id);
    }

    bool
    olg_formula::Item::is_more () const
    {
      if (atom != NULL)
        {
          if (atom->freq == More)
            return true;
          else
            return false;
        }
      if (left != NULL)
        if (!left->is_more ())
          return false;
      if (right != NULL)
        return right->is_more ();
      return false;
    }

    std::string
    olg_formula::Item::to_olg_string () const
    {
      switch (op)
        {
        case aalta_formula::Literal:
          return atom->to_olg_string ();
        case aalta_formula::False:
          return "False";
        case aalta_formula::True:
          return "True";
        case aalta_formula::And:
          return "(" + left->to_olg_string () + " & " + right->to_olg_string () + ")";
        case aalta_formula::Or:
          return "(" + left->to_olg_string () + " | " + right->to_olg_string () + ")";
        case aalta_formula::Not:
          return "!" + atom->to_olg_string ();
        default:
          Printer::printErrorMessage ("Error: olg convert error --- error type " + convertToString (op));
        }
      return "Error";
    }

    std::string
    olg_formula::Item::to_olg_more_string (int pos) const
    {
      std::string l_str, r_str;
      switch (op)
        {
        case aalta_formula::And:
          l_str = left->to_olg_more_string (pos);
          if (l_str == "True")
            return right->to_olg_more_string (pos);
          if (l_str == "False")
            return "False";
          r_str = right->to_olg_more_string (pos);
          if (r_str == "True")
            return l_str;
          if (r_str == "False")
            return "False";
          return "(" + l_str + ") & (" + r_str + ")";
        case aalta_formula::Or:
          l_str = left->to_olg_more_string (pos);
          if (l_str == "True")
            return "True";
          if (l_str == "False")
            return right->to_olg_more_string (pos);
          r_str = right->to_olg_more_string (pos);
          if (r_str == "True")
            return "True";
          if (r_str == "False")
            return l_str;
          return "(" + l_str + ") | (" + r_str + ")";
        case aalta_formula::Literal:
          if ((atom->freq == More && (pos == INT_MAX || atom->pos >= 0 && atom->pos <= pos))
              || (atom->freq == Once && atom->pos == pos))
            return atom->to_olg_string ();
          else
            return "True";
        case aalta_formula::False:
          return "False";
        case aalta_formula::True:
          return "True";
        case aalta_formula::Not:
          if (atom->freq == More && (pos == INT_MAX || atom->pos >= 0 && atom->pos <= pos)
              || atom->freq == Once && atom->pos == pos)
            return "! " + atom->to_olg_string ();
          else
            return "True";
        default:
          Printer::printErrorMessage ("Error: olg convert error --- error type");
        }
      return "";
    }

    std::string
    olg_formula::Item::to_olg_id_string (int id) const
    {
      std::string l_str, r_str;
      switch (op)
        {
        case aalta_formula::And:
          l_str = left->to_olg_id_string (id);
          if (l_str == "True")
            return right->to_olg_id_string (id);
          if (l_str == "False")
            return "False";
          r_str = right->to_olg_id_string (id);
          if (r_str == "True")
            return l_str;
          if (r_str == "False")
            return "False";
          return "(" + l_str + ") & (" + r_str + ")";
        case aalta_formula::Or:
          l_str = left->to_olg_id_string (id);
          if (l_str == "True")
            return "True";
          if (l_str == "False")
            return right->to_olg_id_string (id);
          r_str = right->to_olg_id_string (id);
          if (r_str == "True")
            return "True";
          if (r_str == "False")
            return l_str;
          return "(" + l_str + ") | (" + r_str + ")";
        case aalta_formula::Literal:
          if (atom->pos < 0 && atom->id == id || atom->pos == 0 && atom->freq == More)
            return atom->to_olg_string ();
          else
            return "True";
        case aalta_formula::False:
          return "False";
        case aalta_formula::True:
          return "True";
        case aalta_formula::Not:
          if (atom->pos < 0 && atom->id == -id || atom->pos == 0 && atom->freq == More)
            return "! " + atom->to_olg_string ();
          else
            return "True";
        default:
          Printer::printErrorMessage ("Error: olg convert error --- error type");
        }
      return "";
    }

    std::string
    olg_formula::to_string () const
    {
      if (root == NULL) return "";
      return root->to_string ();
    }

    std::string
    olg_formula::Item::to_string () const
    {
      switch (op)
        {
        case aalta_formula::Literal:
          return atom->to_string ();
        case aalta_formula::False:
          return "False";
        case aalta_formula::True:
          return "True";
        case aalta_formula::And:
          return "(" + left->to_string () + " & " + right->to_string () + ")";
        case aalta_formula::Or:
          return "(" + left->to_string () + " | " + right->to_string () + ")";
        case aalta_formula::Not:
          return "!" + atom->to_string ();
        default:
          Printer::printErrorMessage ("Error: olg convert error --- error type");
        }
      return "Error";
    }

    std::string
    olg_formula::Atom::to_string () const
    {
      //std::string ret = "< " + convertToString (id);
      std::string ret = "< " + aalta_formula::get_name (id);
      if (pos < 0) ret += ", ⊥";
      else ret += ", " + convertToString (pos);
      if (freq == Once) ret += ", −";
      else if (freq == More) ret += ", ≥";
      else ret += ", inf";
      ret += " >";
      return ret;
    }

    void
    olg_formula::destroy (Item* root)
    {
      if (root == NULL) return;
      if (root->atom != NULL)
        delete root->atom;
      destroy (root->left);
      destroy (root->right);
      delete root;
      root = NULL;
    }
  }

