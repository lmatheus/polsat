/* 
 * File:   ba.hh
 * Author: Jianwen Li
 *
 * buchi automaton is a list of states
 * 
 * Created on Aug 26th, 2013
 */
 
 #ifndef BA_HH
 #define BA_HH
 
 #include "state.hh"
 
 namespace spot
 {
   namespace aalta
   {
     class ba
     {
       public:
         //constructor
         ba(aalta_formula*);
         std::string to_promela();
         void set_promela_state_names();
       private:
         state_set states;
     };
   }
 }
 #endif /*BA_HH*/
