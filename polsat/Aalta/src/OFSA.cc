/* 
 * File:   OFOA.cc
 * Author: snowingsea
 * 
 * Created on July 21, 2013, 2:31 PM
 */

#include <map>
#include <iostream>
#include <string.h>
#include <algorithm>
#include "OFSA.hh"
#include "printer.hh"
#include "utility.hh"
#include "olg_formula.hh"
#include "aalta_formula.hh"

using namespace std;


  namespace aalta
  {
    ///////////////////////////////////////////
    /* 静态 */
    timestamp OFSA::dfn;
    timestamp OFSA::low;

    ///////////////////////////////////////////

    /**
     * 将公式按照op分离存于集合中
     * @param op
     * @param af
     * @param s
     */
    void
    OFSA::split2set (int op, aalta_formula* af, edge_set& s)
    {
      if (af == NULL) return;

      if (af->oper () != op)
        {
          s.insert (af);
        }
      else
        {
          split2set (op, af->l_af (), s);
          split2set (op, af->r_af (), s);
        }
    }

    /**
     * 判断一个强联通分支是否可满足
     * @param af
     * @param s
     * @return 
     */
    bool
    OFSA::scc_sat (aalta_formula* af, edge_set& s)
    {
      switch (af->oper ())
        {
        case aalta_formula::And:
          return scc_sat (af->l_af (), s) && scc_sat (af->r_af (), s);
        case aalta_formula::Or:
          return scc_sat (af->l_af (), s) || scc_sat (af->r_af (), s);
        case aalta_formula::Until:
        case aalta_formula::Release:
        case aalta_formula::Next:
          return scc_sat (af->r_af (), s);
        case aalta_formula::True:
          return true;
        case aalta_formula::False:
          return false;
        default:
          return s.find (af) != s.end ();
        }
    }

    ///////////////////////////////////////////

    OFSA::OFSA () { }

    OFSA::~OFSA ()
    {
      for (scc_edge::iterator sit = scc.begin (); sit != scc.end (); sit++)
        delete sit->second;
      scc.clear ();
      dnf_formula::destroy ();
    }

    /**
     * 初始化
     */
    void
    OFSA::init ()
    {
      this->index = 1;
      this->low.clear ();
      this->dfn.clear ();
      while (!this->stk.empty ())this->stk.pop ();
      this->instk.clear ();
      this->result = "";
    }
    
    std::string
    OFSA::get_result()const
    {
      return "(" + this->result + ")";
    }

    /**
     * 判断字符串公式是否可满足
     * @param input
     * @return 
     */
    bool
    OFSA::sat (const std::string& input)
    {
      if (input.empty ())
        return false;
      this->init ();
      /* 公式解析规范化 */
      std::string str = aalta_formula (input).regulate ()->to_string ();
      aalta_formula::destroy ();
      aalta_formula *af = aalta_formula(str).unique ();
      //Printer::printMessage ("af: " + af->to_string ());
      olg_formula olg (af);
      //Printer::printMessage ("\n\nolg: " + olg.to_string ());
      if (olg.sat ())
      {
         this->result = olg.to_olg_string();
         return true;
      }
      if (olg.unsat ())
         return false;
         
      af = af->simply ();
      //Printer::printMessage ("sim: " + af->to_string ());
      return sat (af->classify ());
    }

    /**
     * 判断aalta_formula公式是否可满足
     * @param af
     * @return 
     */
    bool
    OFSA::sat (aalta_formula& af)
    {
      this->init ();
      return sat (af.unique ());
    }

    /**
     * 判断aalta_formula公式指针是否可满足
     * @param afp
     * @return 
     */
    bool
    OFSA::sat (aalta_formula* afp)
    {
      this->init ();
      return this->tarjan (afp->unique ());
    }

    /**
     * tarjan算法寻找强连通分量 O(n+m)
     * @param u
     * @return
     */
    bool
    OFSA::tarjan (aalta_formula* u)
    {
      dfn[u] = low[u] = index++;
      /* 判断该节点是否可满足 */
      switch (u->oper ())
        {
        case aalta_formula::True:
          return true;
        case aalta_formula::False:
          return false;
        default:
          {
            olg_formula olg (u);
            //cout << olg.to_olg_more_string() << endl;
            if (olg.sat ())
            {
              this->result = olg.to_olg_string();
              return true;
            }
            if (olg.unsat ())
              return false;
              
          }
        }

      /* 遍历dnf */
      dnf_formula* dnf = dnf_formula (u).unique ();
      edge_set* es = new edge_set ();
      scc[dnf] = es;
      edge_set::iterator eit;

      this->stk.push (u);
      this->instk.insert (u);
      aalta_formula* v;
      dnf_formula::dnf_clause_set* dc_set = dnf->get_next ();

      int size = dc_set->size ();
      if (size > 0)
        {
          /* 将dnf_clause按照next的公式长度排序 */
          int i = 0;
          dnf_clause* dnf_arr = new dnf_clause[size + 1];
          for (dnf_formula::dnf_clause_set::iterator it = dc_set->begin (); it != dc_set->end (); it++)
            dnf_arr[i++] = *it;
          std::sort (dnf_arr, dnf_arr + size);

          /* 遍历 */
          for (i = 0; i < size; ++i)
            {
              v = dnf_arr[i].next->unique ();
              if (this->dfn.find (v) == this->dfn.end ())
                {
                  if (tarjan (v)) return true;
                  timestamp::iterator u_it = low.find (u);
                  timestamp::iterator v_it = low.find (v);
                  if (u_it->second > v_it->second)
                    u_it->second = v_it->second;
                }
              else if (instk.find (v) != instk.end ())
                {
                  timestamp::iterator u_it = low.find (u);
                  timestamp::iterator v_it = low.find (v);
                  if (u_it->second > v_it->second)
                    u_it->second = v_it->second;
                }
              
              if (low[u] == low[v])
                { // 合并强连通上的边
                  split2set (aalta_formula::And, dnf_arr[i].current, *es);
                  edge_set* next_edge = scc[dnf_formula::get_dnf (v)];
                  for (eit = next_edge->begin (); eit != next_edge->end (); eit++)
                    es->insert (*eit);
                  if (OFSA::scc_sat (u->unique (), *es))
                    return true;
                }
            }
          delete[] dnf_arr;
        }

      if (dfn[u] == low[u])
        {
          do
            {
              v = stk.top ();
              instk.erase (v);
              stk.pop ();
            }
          while (v != u);
        }
      return false;
    }

  }

