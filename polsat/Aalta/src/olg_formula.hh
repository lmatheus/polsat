/* 
 * File:   olg_formula.hh
 * Author: snowingsea
 * 
 * φ = p: ofp(φ) = ⟨p,1,−⟩;
 * φ = φ1 ∧ φ2: ofp(φ) = ofp(φ1) ∧ ofp(φ2);
 * φ = φ1 ∨ φ2: ofp(φ) = ofp(φ1) ∨ ofp(φ2);
 * φ = X φ1: ofp(φ) = Pos(ofp(φ1), X );
 * φ = Gφ1: ofp(φ) = Pos(ofp(φ1),G);
 * φ = φ1Uφ2: ofp(φ) = Pos(ofp(φ2), U);
 * φ = φ1Rφ2 (φ1 != ff): ofp(φ) = Pos(ofp(φ2), R);
 * 
 * Created on July 31, 2013, 6:17 PM
 */

#ifndef OLG_FORMULA_HH
#define	OLG_FORMULA_HH

#include "aalta_formula.hh"
#include "printer.hh"
#include "utility.hh"

#include <list>
//@TODO:重构之
  namespace aalta
  {

    class olg_formula
    {
    private:

      enum freqkind
      {
        Once, More, Inf
      };

      struct Atom
      {
        int id;
        int pos;
        freqkind freq;

        Atom (int id, int pos, freqkind freq) : id (id), pos (pos), freq (freq) { }

        std::string to_string ()const;
        std::string to_olg_string ()const;
      };

      struct Item
      {
        aalta_formula::opkind op;
        Atom* atom;
        Item* left;
        Item* right;
        int compos;  /*added by Jianwen Li on Sept. 19th, 2013
                      record the common pos of all its sub atoms, if the pos exists. 
                      Otherwise compos = -1*/
        Item (aalta_formula::opkind op, Item* left = NULL, Item* right = NULL, Atom* atom = NULL) : op (op), left (left), right (right), atom (atom) { }

        std::string to_string ()const;
        std::string to_olg_string ()const;
        std::string to_olg_more_string (int pos)const;
        std::string to_olg_id_string (int id)const;
        bool is_more()const;
      };

    private:
      Item* root;
      std::list<Item*> GF;
      std::list<Item*> G;
      std::list<Item*> NGF;
      std::list<Item*> ALLG;
      

      std::list<int> oplist;
      int* pos_arr;
      int pos_size;
      int* id_arr;
      int id_size;

    public:
      olg_formula (aalta_formula* af);
      virtual ~olg_formula ();

      bool sat ();
      bool unsat ();

      std::string to_string ()const;
      std::string to_olg_string ()const;

      bool static sat (const std::string& formula);

    private:
      Item* build (aalta_formula* af);
      void split(aalta_formula* af);
      void destroy (Item* root);
      int get_more_max_pos()const;
      /*
       changed by Jianwen Li
       On Sept. 19th, 2013
      */
      //void add_pos (Atom* atom);
      void add_pos(Item* , aalta_formula::opkind);
      void col_info (Item* root);
      
    };
  }

#endif	/* OLG_FORMULA_HH */

