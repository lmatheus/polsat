/* 
 * File:   dnf_clause.hh
 * Author: snowingsea
 *
 * Created on July 20, 2013, 10:53 AM
 */

#ifndef DNF_CLAUSE_HH
#define	DNF_CLAUSE_HH

#include "aalta_formula.hh"


  namespace aalta
  {

    class dnf_clause
    {
    public:

      /* dnf_clause的hash函数 */
      struct dnf_clause_hash
      {

        size_t operator () (const dnf_clause& clause) const
        {
          return clause.hash;
        }
      };

      ///////////
      //成员变量//
      //////////////////////////////////////////////////
    private:
      size_t hash;              // hash值
    public:
      aalta_formula* current;   // 当前aalta_formula
      aalta_formula* next;      //下一个节点aalta_formula
      //////////////////////////////////////////////////

      //------------------------------------------------
      /* 成员方法 */
    public:

      dnf_clause (aalta_formula* current = NULL, aalta_formula* next = NULL);
      dnf_clause (const dnf_clause& orig);

      dnf_clause& operator = (const dnf_clause& dc);
      bool operator == (const dnf_clause& dc)const;
      bool operator< (const dnf_clause& dc)const;

      std::string to_string ()const;

    private:
      void clc_hash ();
    };
  }


#endif	/* DNF_CLAUSE_HH */

