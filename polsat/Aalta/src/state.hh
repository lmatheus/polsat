/* 
 * File:   state.hh
 * Author: Jianwen Li
 *
 * state = <af, P> where P is a subset of L.(set of literals)
 * 
 * Created on Aug 25th, 2013
 */

#ifndef STATE_HH
#define	STATE_HH

#include "aalta_formula.hh"
#include "OFSA.hh"
#include "tran.hh"


  namespace aalta
  {
    class state
    {
    public:
      //constructor
      state(aalta_formula*);
      state(aalta_formula*, edge_set);
      //getter
      size_t get_id() {return id;}
      edge_set get_P() {return P;}
      bool is_init() {return init;}
      bool is_acc() {return acc;}
      std::string get_promela_name() {return promela_name;}
      
      //setter
      void set_id(size_t num) {id = num;}
      void set_P(edge_set P) {this->P = P;}
      void set_init(bool b)  {this->init = b;}
      void set_acc(bool a)  {this->acc = a;}
      void set_trans(tran_set trans) {this->trans = trans;}
      void set_promela_name(std::string name) {this->promela_name = name;} 
      
      //destroy function
      void destroy()
      {
        if(af != NULL)
          af->destroy(); 
        edge_set::iterator it;
        for(it = P.begin(); it != P.end(); it ++)
        {
          if((*it) != NULL)
            (*it)->destroy();
        }
        tran_set::iterator it;
        for(it = trans.begin(); it != trans.end(); it ++)
        {
          if((*it) != NULL)
            (*it)->destroy();
        }
        delete this;
       } 
      
      std::string to_string();
      
      std::string to_promela();
      
      struct st_hash
      {
        size_t operator () (const state* st) const
        {
          return st->hash;
        }
      }
      
      struct ptr_st
      {
        bool operator () (const state* st1, const state* st2) const;
      }
      
      typedef Sgi::hash_set<state*, st_hash, ptr_st> state_set;
      
    private:
      size_t id;
      aalta_formula *af;
      edge_set P;
      size_t hash;
      bool init;
      bool acc;
      tran_set trans; //the transition set;
      std::string promela_name;
    };
  }

#endif	/* STATE_HH */

