/* 
 * File:   OFOA.hh
 * Author: snowingsea
 * 
 * On-the-fly Satisfiability Algorithm
 * 
 * Created on July 21, 2013, 2:31 PM
 */

#ifndef OFOA_HH
#define	OFOA_HH

#include "dnf_formula.hh"

#include <set>
#include <list>
#include <stack>


  namespace aalta
  {

    class OFSA
    {
    private:
      
      
//      typedef std::set<aalta_formula*> dnf_set;
//      typedef std::set<aalta_formula*> afp_set;
//      typedef std::set<aalta_formula*> edge_set;
//      typedef std::map<aalta_formula*, int> timestamp;
//      typedef std::map<dnf_formula*, edge_set*> scc_edge;

      ///////////
      //成员变量//
      //////////////////////////////////////////////////
    private:
      int index;                        //节点访问次序
      static timestamp low;             //节点搜索的次序编号(时间戳)
      static timestamp dfn;             //节点或节点的子树能够追溯到的最早的栈中节点的次序号
      std::stack<aalta_formula*> stk;   //栈
      dnf_set instk;                    // 记是否在栈中
      std::string result;

      scc_edge scc;                     //记录scc边的信息
      //////////////////////////////////////////////////

      //------------------------------------------------
      /* 成员方法 */
    public:
      OFSA ();
      virtual ~OFSA ();

      bool sat (const std::string& input);
      bool sat (aalta_formula& af);
      bool sat (aalta_formula* afp);
      
      std::string get_result()const;

    private:
      void init ();

      bool tarjan (aalta_formula* dnf);
      static bool scc_sat(aalta_formula* af, edge_set& s);
      static void split2set(int op, aalta_formula* af, edge_set& s);
    };
  }

#endif	/* OFOA_HH */

