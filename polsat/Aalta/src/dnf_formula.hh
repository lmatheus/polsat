/* 
 * File:   dnf_formula.hh
 * Author: snowingsea
 *
 * DNF(id) = DNF(left) U DNF(right)
 * 
 * 1. DNF(α) = {α ∧ X(True)} where α is a literal;
 * 2. DNF(Xφ) = {True∧X(φ)};
 * 3. DNF(φ1Uφ2) = DNF(φ2) ∪ DNF(φ1 ∧ X(φ1Uφ2));
 * 4. DNF(φ1Rφ2) = DNF(φ1 ∧ φ2) ∪ DNF(φ2 ∧ X(φ1Rφ2));
 * 5. DNF(φ1∨φ2) = DNF(φ1) ∪ DNF(φ2);
 * 6. DNF(φ1∧φ2) = {(α1∧α2)∧X(ψ1∧ψ2) | ∀i = 1,2.αi∧X(ψi) ∈ DNF(φi)};
 * 
 * Created on July 20, 2013, 11:28 AM
 */

#ifndef DNF_FORMULA_HH
#define	DNF_FORMULA_HH

#include "dnf_clause.hh"
#include "aalta_formula.hh"


  namespace aalta
  {

    class dnf_formula
    {
    public:
//      typedef std::set<dnf_clause> dnf_clause_set;
//      typedef std::set<aalta_formula*> next_set;
//      typedef std::map<aalta_formula*, dnf_formula*> af_dnf_map;
//      typedef std::map<aalta_formula*, dnf_clause_set*> dnf_map;
      typedef Sgi::hash_set<dnf_clause, dnf_clause::dnf_clause_hash> dnf_clause_set;
      typedef Sgi::hash_set<aalta_formula*> next_set;
      typedef Sgi::hash_map<aalta_formula*, dnf_formula*> af_dnf_map;
      typedef Sgi::hash_map<aalta_formula*, dnf_clause_set*> dnf_map;

      //////////////
      //静态成员变量//
      //////////////////////////////////////////////////
    private:
      static af_dnf_map all_dnfs;       //记录所有dnf
      static dnf_map atomic_dnfs;       //保存dnf叶子id和对应的dnf_clause集合
      static aalta_formula AF;          //初始化aalta_formula, 无实质用处
      //////////////////////////////////////////////////

      ///////////
      //成员变量//
      //////////////////////////////////////////////////
    private:
      aalta_formula* id;        // 将公式对应的唯一aalta_formula指针作为dnf的id标识
      dnf_formula* left;        // 并集左节点
      dnf_formula* right;       // 并集右节点
      dnf_formula* unique_prt;  // 对应的dnf唯一标识

      //------------------------------------------------
      /* 成员方法 */
    public:
      dnf_formula ();
      dnf_formula (const std::string& input);
      dnf_formula (aalta_formula& af);
      dnf_formula (aalta_formula* af);
      dnf_formula (const dnf_formula& orig);

      dnf_formula& operator = (const dnf_formula& dnf);

      dnf_formula* unique ();
      dnf_formula* clone ()const;
      std::string to_string ()const;

      aalta_formula* get_id ()const;
      dnf_clause_set* get_next ()const;
      void get_next (dnf_clause_set* dc_set)const;
      next_set* get_all_next ();

      static void destroy ();
      static dnf_formula* get_dnf (aalta_formula* af);
      static dnf_clause_set* get_next (aalta_formula* af);

    private:
      void init ();
      void build ();
      void cross (const dnf_formula* dnf1, dnf_formula* dnf2, dnf_clause_set* s);
      dnf_clause_set* find_next ();
    };
    
    typedef Sgi::hash_map<dnf_formula*, edge_set*> scc_edge;
  }


#endif	/* DNF_FORMULA_HH */

