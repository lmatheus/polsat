%{


/* This is a parser for LTL formulas in in-fix order */

//#include "PANDA.h"

#include "public.hh"
    





%}
%code requires
{  
  #include "aalta_formula.hh"
  using namespace aalta;
}

%parse-param {aalta::aalta_formula* &result}

%union {
//    struct node *nPtr;		/* node pointer */
    aalta::aalta_formula* af;
    char* varName;              /* name of a variable */
};

%code{
  #include "parsedecl.hh"
}

%token AND OR NOT IMPLIES NEXT UNTIL GLOBALLY LPAREN RPAREN EQUIV
%token <varName> PROP 
%token FFALSE TTRUE

/* Establish precedence rules and left-associativity*/
/* (different from SMV -- I think ~ should bind tighter than U) */
%left RELEASE      /*  release  */
%left UNTIL        /*   until   */
%left IMPLIES      /*     ->    */
%left EQUIV        /*     <->    */
%left OR           /*     or    */
%left AND          /*    and    */
%nonassoc GLOBALLY /*    globally   */
%nonassoc FUTURE   /* in the future */
%nonassoc NEXT     /*   next time   */
%nonassoc NOT      /*      not      */
%nonassoc PROP
%nonassoc TTRUE
%nonassoc FFALSE

%type <af> input formula

%%



input: formula { /*a simple top-level to assign the root*/
    result = $1;
    /*fprintf(stderr, "Finished parsing formula\n");*/
}
;

formula: 
PROP { 
    /*printf("Found a leaf\n"); */

    result = new aalta_formula(aalta_formula::Literal, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    std::string str(yylval.varName);
    result -> set_name(str);
    $$ = result;
    
}
| TTRUE { 
    /*printf("Found a TRUE\n"); */
    
    result = new aalta_formula(aalta_formula::True, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    
    result -> set_name("TRUE");
    $$ = result;
    
}
| FFALSE { 
  /*fprintf(stderr, "Found a FALSE\n");*/
  
    result = new aalta_formula(aalta_formula::False, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("FALSE");
    $$ = result;
  
}
| NOT formula {
    /*printf("Found a '~' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Not, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("!");
    result -> set_right($2);
    $$ = result;
    
}
| NEXT formula { 
    /*printf("Found a 'X' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Next, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("X");
    result -> set_right($2);
    $$ = result;
    
}
| GLOBALLY formula {
    /*printf("Found a 'G' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Global, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("G");
    result -> set_right($2);
    $$ = result;
    
} 
| FUTURE formula {
    /*printf("Found an 'F' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Future, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("F");
    result -> set_right($2);
    $$ = result;
    
} 
| formula AND formula {
    /*printf("Found a '&' operator\n"); */
    
    result = new aalta_formula(aalta_formula::And, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("&");
    result -> set_left($1);
    result -> set_right($3);
    $$ = result;
    
   
}
| formula OR formula {
    /*fprintf(stderr, "Found a '|' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Or, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("|");
    result -> set_left($1);
    result -> set_right($3);
    $$ = result;
    
}
| formula IMPLIES formula {
    /*printf("Found a '->' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Imply, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("->");
    result -> set_left($1);
    result -> set_right($3);
    $$ = result;
    
}
| formula EQUIV formula {
    result = new aalta_formula(aalta_formula::Equiv, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("<->");
    result -> set_left($1);
    result -> set_right($3);
    $$ = result;
}
| formula UNTIL formula {
    /*printf("Found a 'U' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Until, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("U");
    result -> set_left($1);
    result -> set_right($3);
    $$ = result;
    
}
| formula RELEASE formula {
    /*printf("Found an 'R' operator\n"); */
    
    result = new aalta_formula(aalta_formula::Release, NULL, NULL);
    if (result == NULL)
    { 
      fprintf(stderr, "Memory error11\n"); 
      exit(1); 
    }
    result -> set_name("R");
    result -> set_left($1);
    result -> set_right($3);
    $$ = result;
    
}
| LPAREN formula RPAREN { 
    /*printf("Found parens!\n"); */
    $$ = $2;
}
;

%%

void
yy::parser::error(const location_type& location, const std::string& message)
{
  std::cout << message << std::endl;
  printInput();
  exit(0);
}

namespace aalta{
  aalta_formula* parse(const std::string& input)
  {
    flex_set_buffer(input.c_str());
    aalta_formula *result = 0;
    yy::parser parser(result);
    parser.parse();
    return result;
  }
}

/*
int main(int argc, char** argv)
{
  std::string input;
  std::getline(std::cin, input);
  aalta::aalta_formula* af = aalta::parse(input);
  std::cout << af->to_string() << std::endl;
  return 0;
}
*/


