/* 
 * File:   ba.hh
 * Author: Jianwen Li
 *
 * buchi automaton is a list of states
 * 
 * Created on Aug 26th, 2013
 */
 
 #include "aalta_formula.hh"
 #include "ba.hh"
 
 namespace spot
 {
   namespace aalta
   {
     ba::ba(aalta_formula* af)
     {
       
     }
     
     std::string ba::to_promela()
     {
       std::string result = "never{\n";
       state_set::iterator it;
       for(it = states.begin(); it != states.end(); it ++)
       {
         result += (*it)->to_promela();
       }
       result += "}\n";
       return result;
     }
     
     void ba::set_promela_states_names()
     {
       state_set::iterator it;
       int count = 0;
       bool init, acc;
       size_t id;
       for(it = states.begin(); it != states.end(); it ++)
       {
         (*it)->set_id(count++);
         
         init = (*it)->get_init();
         acc = (*it)->get_acc();
         id = (*it)->get_id();
         if(init && acc)
         {
           (*it)->set_promela_name("init_accept");
         }
         else
         {
           if(init)
           {
             (*it)->set_promela_name("init_S" + convertToStrin(id));
       
           }
           else
           {
             if(acc)
             {
               (*it)->set_promela_name("accept_S" + convertToString(id));
             }
             else
             {
               (*it)->set_promela_name("S" + convertToStrin(id));
             }
           }
         }
       }
       
     }
     
     
     
     
   }
 }
