

#ifndef PARSEDECL_HH
#define PARSEDECL_HH

//#include "parsedecl.hh"
#include "location.hh"


# define YY_DECL \
  int yylex (yy::parser::semantic_type *yylval)
YY_DECL;

void flex_set_buffer(const char *buf);
void flex_unset_buffer();

#endif // SPOT_LTLPARSE_PARSEDECL_HH
