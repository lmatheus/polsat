/* 
 * File:   dnf_formula.cc
 * Author: snowingsea
 * 
 * Created on July 20, 2013, 11:28 AM
 */

#include "dnf_formula.hh"
#include "utility.hh"
#include "printer.hh"


  namespace aalta
  {
    ///////////////////////////////////////////
    /* 初始化静态变量 */
    dnf_formula::af_dnf_map dnf_formula::all_dnfs;
    dnf_formula::dnf_map dnf_formula::atomic_dnfs;
    aalta_formula dnf_formula::AF;

    /**
     * 通过af指针标识得到对应dnf的next集合
     * @param af
     * @return 
     */
    dnf_formula::dnf_clause_set*
    dnf_formula::get_next (aalta_formula* af)
    {
      dnf_map::iterator it = atomic_dnfs.find (af->unique ());
      if (it == atomic_dnfs.end ())
        return NULL;
      return it->second;
    }

    /**
     * 通过af指针标识得到对应的dnf
     * @param af
     * @return 
     */
    dnf_formula*
    dnf_formula::get_dnf (aalta_formula* af)
    {

      af_dnf_map::const_iterator it = all_dnfs.find (af);
      if (it != all_dnfs.end ())
        return it->second;
      return NULL;
    }

    /**
     * 销毁静态变量, 释放资源
     */
    void
    dnf_formula::destroy ()
    {
      aalta_formula::destroy ();
      for (af_dnf_map::iterator it = all_dnfs.begin (); it != all_dnfs.end (); it++)
        delete it->second;
      all_dnfs.clear ();
      for (dnf_map::iterator it = atomic_dnfs.begin (); it != atomic_dnfs.end (); it++)
        delete it->second;
      atomic_dnfs.clear ();
    }

    ///////////////////////////////////////////

    /**
     * 初始化成员变量
     */
    void
    dnf_formula::init ()
    {
      this->left = this->right = this->unique_prt = NULL;
      this->id = NULL;
    }

    dnf_formula::dnf_formula ()
    {
      this->init ();
    }

    dnf_formula::dnf_formula (const std::string& input)
    {
      this->init ();
      /* 公式解析规范化 */
      std::string str = aalta_formula (input).regulate ()->to_string ();
      aalta_formula::destroy ();
      /* 此处第二次调用spot */
      aalta_formula* af = aalta_formula(str).unique ();
      af = af->simply ();
      this->id = af->classify ();
      this->build ();
    }

    dnf_formula::dnf_formula (aalta_formula& af)
    {
      this->init ();
      this->id = af.unique ();
      this->build ();
    }

    dnf_formula::dnf_formula (aalta_formula* af)
    {
      this->init ();
      this->id = af->unique ();
      this->build ();
    }

    dnf_formula::dnf_formula (const dnf_formula& orig)
    {
      *this = orig;
    }

    /**
     * 重载赋值操作符
     * @param dnf
     * @return 
     */
    dnf_formula& dnf_formula::operator = (const dnf_formula& dnf)
    {
      if (this != &dnf)
        {
          this->id = dnf.id;
          this->left = dnf.left;
          this->right = dnf.right;
          this->unique_prt = dnf.unique_prt;
        }
      return *this;
    }

    void
    dnf_formula::build ()
    {
      af_dnf_map::const_iterator it = all_dnfs.find (this->id);
      if (it != all_dnfs.end ())
        {
          *this = *(it->second);
          return;
        }
      switch (this->id->oper ())
        {
        case aalta_formula::Next:
          { // DNF(X φ)={True ∧ X(φ)}
            dnf_clause_set* dc_set = this->find_next ();
            if (dc_set->empty ())
              dc_set->insert (dnf_clause (aalta_formula::TRUE (id->get_tag ()), id->r_af ()->unique ()));
            break;
          }
        case aalta_formula::Until:
          { // DNF(φ1 U φ2) = DNF(φ1 ∧ X(φ1 U φ2)) ∪ DNF(φ2)
            aalta_formula* afp = aalta_formula (aalta_formula::Next, NULL, this->id, this->id->get_tag ()).unique ();
            afp = aalta_formula (aalta_formula::And, this->id->l_af (), afp, this->id->get_tag ()).simply_and ();
            this->left = dnf_formula (afp).unique ();
            this->right = dnf_formula (this->id->r_af ()).unique ();
            break;
          }
        case aalta_formula::Release:
          { // DNF(φ1 R φ2) = DNF(φ1 ∧ φ2) ∪ DNF(φ2 ∧ X(φ1 R φ2))
            aalta_formula* l_afp = aalta_formula (aalta_formula::And,
                                                  id->l_af ()->unique (),
                                                  id->r_af ()->unique (),
                                                  id->get_tag ()).simply_and ();
            aalta_formula* r_afp = aalta_formula (aalta_formula::Next, NULL, id->unique (), id->get_tag ()).unique ();
            r_afp = aalta_formula (aalta_formula::And, id->r_af ()->unique (), r_afp, id->get_tag ()).simply_and ();
            this->left = dnf_formula (l_afp).unique ();
            this->right = dnf_formula (r_afp).unique ();
            break;
          }
        case aalta_formula::Or:
          { // DNF(φ1 ∨ φ2) = DNF(φ1) ∪ DNF(φ2)
            this->left = dnf_formula (this->id->l_af ()->unique ()).unique ();
            this->right = dnf_formula (this->id->r_af ()->unique ()).unique ();
            break;
          }
        case aalta_formula::And:
          { // DNF(φ1∧φ2) = {(α1∧α2)∧X(ψ1∧ψ2) | ∀i = 1,2.αi∧X(ψi) ∈ DNF(φi)}
            dnf_clause_set* dc_set = this->find_next ();
            if (dc_set->empty ())
              {
                dnf_formula* l_dnf = dnf_formula (this->id->l_af ()).unique ();
                dnf_formula* r_dnf = dnf_formula (this->id->r_af ()).unique ();
                this->cross (l_dnf, r_dnf, dc_set);
              }

            break;
          }
        case aalta_formula::Literal:
        case aalta_formula::Undefined:
          {
            Printer::printErrorMessage ("the formula cannot be transformed to dnf_formula");
            break;
          }
        default:
          { // DNF(α) = {α ∧ X(True)}
            dnf_clause_set* dc_set = this->find_next ();
            if (dc_set->empty () && id->oper () != aalta_formula::False)
              dc_set->insert (dnf_clause (id->unique (), aalta_formula::TRUE (id->get_tag ())));
            break;
          }
        }
    }

    /**
     * DNF(φ1∧φ2) = {(α1∧α2)∧X(ψ1∧ψ2) | ∀i = 1,2.αi∧X(ψi) ∈ DNF(φi)}
     * @param dnf1
     * @param dnf2
     * @param s
     */
    //@ TODO: 考虑优化
    void
    dnf_formula::cross (const dnf_formula* dnf1, dnf_formula* dnf2, dnf_clause_set* s)
    {
      if (dnf1 == NULL || dnf2 == NULL)
        {
          Printer::printErrorMessage ("Error: formula in cross is NULL!");
          return;
        }
      if (dnf1->left != NULL)
        {
          cross (dnf1->left, dnf2, s);
          cross (dnf1->right, dnf2, s);
        }
      else if (dnf2->left != NULL)
        {
          cross (dnf1, dnf2->left, s);
          cross (dnf1, dnf2->right, s);
        }
      else
        {
          dnf_clause_set* s1 = atomic_dnfs[dnf1->id];
          dnf_clause_set* s2 = atomic_dnfs[dnf2->id];
          dnf_clause_set::const_iterator it1, it2;
          for (it1 = s1->begin (); it1 != s1->end (); it1++)
            for (it2 = s2->begin (); it2 != s2->end (); it2++)
              {
                aalta_formula* caf = aalta_formula (aalta_formula::And, it1->current, it2->current, id->get_tag ()).simply_and_weak ();
                if (caf->oper () == aalta_formula::False)
                  continue;
                aalta_formula* naf = aalta_formula (aalta_formula::And, it1->next, it2->next, id->get_tag ()).simply_and ();
                s->insert (dnf_clause (caf, naf));
              }
        }
    }

    /**
     * 通过aalta_formula指针标识寻找next集合 
     * 若之前未生成过该标识的next集合, 将new一个空集合加入到atomic_dnfs中去
     * @param id
     * @return 
     */
    dnf_formula::dnf_clause_set*
    dnf_formula::find_next ()
    {
      dnf_map::const_iterator it = atomic_dnfs.find (this->id);
      if (it == atomic_dnfs.end ())
        {
          dnf_clause_set* tmp = new dnf_clause_set ();
          atomic_dnfs[this->id] = tmp;
          this->left = this->right = NULL;
          return tmp;
        }
      return it->second;
    }

    /**
     * 返回该dnf_formula对应的唯一指针标识
     * @return 
     */
    dnf_formula*
    dnf_formula::unique ()
    {
      if (this->unique_prt == NULL)
        {
          af_dnf_map::const_iterator it = all_dnfs.find (this->id);
          if (it != all_dnfs.end ())
            this->unique_prt = it->second;
          else
            {
              this->unique_prt = this->clone ();
              all_dnfs[this->id] = this->unique_prt;
            }
        }
      return this->unique_prt;
    }

    /**
     * 克隆出该对象的副本（需要在外部显式delete）
     * @return 
     */
    dnf_formula*
    dnf_formula::clone () const
    {
      return new dnf_formula (*this);
    }

    /**
     * 获取唯一aalta_formula指针标识
     * @return 
     */
    aalta_formula*
    dnf_formula::get_id () const
    {
      return this->id;
    }

    /**
     * 获取next集合
     * @return 
     */
    dnf_formula::dnf_clause_set*
    dnf_formula::get_next () const
    {
      dnf_map::iterator it = atomic_dnfs.find (this->id);
      if (it == atomic_dnfs.end ())
        {
          atomic_dnfs[this->id] = new dnf_clause_set ();
          it = atomic_dnfs.find (this->id);
        }
      if (it->second->empty ())
        {
          if (this->left != NULL)
            this->left->get_next (it->second);
          if (this->right != NULL)
            this->right->get_next (it->second);
        }
      return it->second;
    }

    /**
     * 获取next集合
     * @param dc_set
     */
    void
    dnf_formula::get_next (dnf_clause_set* dc_set) const
    {
      dnf_map::iterator it = atomic_dnfs.find (this->id);
      if (it == atomic_dnfs.end ())
        {
          this->left->get_next (dc_set);
          this->right->get_next (dc_set);
        }
      else
        {
          for (dnf_clause_set::const_iterator iter = it->second->begin (); iter != it->second->end (); iter++)
            dc_set->insert (*iter);
        }
    }

    dnf_formula::next_set*
    dnf_formula::get_all_next ()
    {
      this->unique ();
      next_set* next = new next_set ();
      dnf_clause_set::const_iterator sit;
      af_dnf_map::const_iterator mit;
      next_set::iterator begin;
      dnf_clause_set* s;
      next_set tmp;
      tmp.insert (this->id->unique ());
      while (!tmp.empty ())
        {
          begin = tmp.begin ();
          next->insert (*begin);
          mit = all_dnfs.find (*begin);
          if (mit == all_dnfs.end ())
            s = dnf_formula (*begin).unique ()->get_next ();
          else
            s = mit->second->get_next ();
          tmp.erase (begin);
          for (sit = s->begin (); sit != s->end (); sit++)
            {
              if (next->find (sit->next) == next->end ())
                tmp.insert (sit->next);
            }
          Printer::printMessage (convertToString (tmp.size ()));
        }
      return next;
    }

    /**
     * To String
     * @return 
     */
    std::string
    dnf_formula::to_string () const
    {
      dnf_clause_set* dc_set = this->get_next ();

      std::string ret;
      ret += this->id->to_string () + " [" + convertToString (dc_set->size ()) + "]";


      if (dc_set->size () > 0)
        {
          ret += "\n{";
          for (dnf_clause_set::const_iterator it = dc_set->begin (); it != dc_set->end (); it++)
            ret += "\n\t" + it->to_string ();
          ret += "\n}";
        }

      ret += "\n-----";

      return ret;
    }
  }
