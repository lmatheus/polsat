/* 
 * File:   state.cc
 * Author: Jianwen Li
 *
 * state = <af, P> where P is a subset of L.(set of literals)
 * 
 * Created on Aug 25th, 2013
 */
 
 #include "state.hh"
 

    namespace aalta
    {
       state::state(aalta_formula *af)
       {
         this->id = -1;
         this->af = af;
         this->init = false;
         this->acc = false;
         
       }
       
       state::state(aalta_formula *af, edge_set P)
       {
         this->id = -1;
         this->af = af;
         this->init = false;
         this->acc = false;
         this->P = P;
       }
       
       std::string state::to_string()
       {
         std::string result = "<";
         result += af->to_string() + ", " + to_string(P) + ">";
         return result;
       }
       
       std::string state::to_promela()
       {
         std::string result = "";
         
         
         tran_set::iterator it;
         for(it = trans.begin(); it != trans.end(); it ++)
         {
           result += "\t" + (*it)->promela();
         }
         return result;
       }      
       
    }

 
