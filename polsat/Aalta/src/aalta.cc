#include <string.h>
#include <sys/time.h>
#include <fstream>
#include <iostream>

#include "aalta_formula.hh"
#include "dnf_formula.hh"
#include "printer.hh"
#include "utility.hh"
#include "OFSA.hh"
#include "olg_formula.hh"
#include "public.hh"

using namespace std;

#define MAXN 100000

char in[MAXN];
char ans[MAXN];

double
currentTime ()
{
  struct timeval tv;
  gettimeofday (&tv, NULL);
  return tv.tv_sec + tv.tv_usec / 1000000.0;
}

/*
void
test_af ()
{
  FILE* fin = fopen ("test_data/af_test.in", "r");
  FILE* fans = fopen ("test_data/af_test.ans", "r");
  if (fin == NULL || fans == NULL)
    {
      aalta::Printer::printMessage ("open file error");
      return;
    }
  while (fgets (in, MAXN, fin))
    {
      if (in[strlen (in) - 1] == '\n')
        in[strlen (in) - 1] = '\0';
      if (strlen (in) == 0)continue;
      do
        {
          fgets (ans, MAXN, fans);
          if (ans[strlen (ans) - 1] == '\n')
            ans[strlen (ans) - 1] = '\0';
        }
      while (strlen (ans) == 0);
      aalta::aalta_formula af (in);
      if (af.to_string () == std::string (ans))
        {
          aalta::Printer::printMessage ("Passed : " + std::string (in) + " → " + af.spot_formula_to_string (in));
        }
      else
        {
          aalta::Printer::printMessage ("Fail : " + std::string (in) + " → " + af.spot_formula_to_string (in));
          aalta::Printer::printMessage ("Expect answer: " + std::string (ans));
          aalta::Printer::printMessage ("But receive: " + af.to_string ());
          aalta::Printer::printMessage ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");
        }
    }
  aalta::aalta_formula::destroy ();
  fclose (fin);
  fclose (fans);
}

void
test_af_simply ()
{
  FILE* fin = fopen ("test_data/af_simply.in", "r");
  if (fin == NULL)
    {
      aalta::Printer::printMessage ("open file error");
      return;
    }
  while (fgets (in, MAXN, fin))
    {
      if (in[strlen (in) - 1] == '\n')
        in[strlen (in) - 1] = '\0';
      if (strlen (in) == 0)
        continue;
      do
        {
          fgets (ans, MAXN, fin);
          if (ans[strlen (ans) - 1] == '\n')
            ans[strlen (ans) - 1] = '\0';
        }
      while (strlen (ans) == 0);
      aalta::aalta_formula af (in);

      if (af.simply () == aalta::aalta_formula (ans).unique ())
        {
          aalta::Printer::printMessage ("Passed : " + std::string (in) + " → " + af.spot_formula_to_string (in));
        }
      else
        {
          aalta::Printer::printMessage ("Fail : " + std::string (in) + " → " + af.spot_formula_to_string (in));
          aalta::Printer::printMessage ("Expect answer: " + aalta::aalta_formula (ans).to_string ());
          aalta::Printer::printMessage ("But receive: " + af.simply ()->to_string ());
          aalta::Printer::printMessage ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");
        }
    }
  aalta::aalta_formula::destroy ();
  fclose (fin);
}

void
test_dnf ()
{
  FILE* fin = fopen ("test_data/dnf_test.in", "r");
  if (fin == NULL)
    {
      aalta::Printer::printMessage ("open file error");
      return;
    }

  while (fgets (in, MAXN, fin))
    {
      if (in[strlen (in) - 1] == '\n')
        in[strlen (in) - 1] = '\0';
      if (strlen (in) == 0)
        continue;
      aalta::dnf_formula dnf (in);
      aalta::Printer::printMessage (dnf.to_string ());
    }

  //  aalta::Printer::printMessage (aalta::aalta_formula::all_to_string ());
  aalta::dnf_formula::destroy ();
  fclose (fin);
}

void
test_dnf_next ()
{
  clock_t tol = clock ();
  FILE* fin = fopen ("test_data/dnf_test.in", "r");
  if (fin == NULL)
    {
      aalta::Printer::printMessage ("open file error");
      return;
    }

  while (fgets (in, MAXN, fin))
    {
      if (in[strlen (in) - 1] == '\n')
        in[strlen (in) - 1] = '\0';
      if (strlen (in) == 0)
        continue;
      clock_t st = clock ();
      aalta::aalta_formula af (in);
      printf ("af: %s\n", af.to_string ().c_str ());
      printf ("af_sim: %s\n", af.simply ()->to_string ().c_str ());
      break;
      aalta::dnf_formula dnf (in);
      printf ("dnf: %s\n", dnf.get_id ()->to_string ().c_str ());
      aalta::dnf_formula::next_set* next = dnf.get_all_next ();
      //      for (aalta::dnf_formula::next_set::const_iterator it = next->begin (); it != next->end (); it++)
      //        printf ("%s\n", aalta::dnf_formula (*it).to_string ().c_str ());
      printf ("next_size: %s\n", convertToString (next->size ()).c_str ());
      delete next;
      printf ("time :  %.6fs\n-------\n", ((double) clock () - st) / CLOCKS_PER_SEC);
      break;
    }
  //  aalta::Printer::printMessage (aalta::aalta_formula::all_to_string ());
  aalta::dnf_formula::destroy ();
  fclose (fin);
  printf ("total time :  %.6fs\n", ((double) clock () - tol) / CLOCKS_PER_SEC);
}

void
test_OFOA ()
{
  FILE* fin = fopen ("test_data/in", "r");
  if (fin == NULL)
    {
      aalta::Printer::printMessage ("open file error");
      return;
    }

  while (fgets (in, MAXN, fin))
    {
      if (in[strlen (in) - 1] == '\n')
        in[strlen (in) - 1] = '\0';
      if (strlen (in) == 0)
        continue;

      double st = currentTime ();
      aalta::aalta_formula af (in);
      printf ("in: %s\n", in);
      printf ("af: %s\n", af.to_string ().c_str ());
      printf ("sim: %s\n", af.simply ()->to_string ().c_str());
      printf ("length: %d\n",af.get_length ());      
//      printf ("af_sim: %s\n", af.simply ()->to_string ().c_str ());
//      break;
      aalta::OFSA ofoa;
      printf ("%s\n", ofoa.sat (in) ? "SAT" : "UNSAT");
      printf ("time :  %.9fs\n-------\n", currentTime () - st);
      break;
    }
  fclose (fin);
}

void
test_aalta ()
{
  for (int i = 1; i < 2; i++)
    {
      FILE* fin = fopen ((convertToString ("test_data/sat/N") + convertToString (i) + ".form").c_str (), "r");
      FILE* fans = fopen ((convertToString ("test_data/sat/N") + convertToString (i) + ".form.result").c_str (), "r");

      int index = 0;
      double time;

      aalta::OFSA ofoa;
      while (fgets (in, MAXN, fin))
        {
          if (in[strlen (in) - 1] == '\n')
            in[strlen (in) - 1] = '\0';
          if (strlen (in) == 0)
            continue;

          fscanf (fans, "%d%lf%s", &index, &time, ans);
          printf ("%d", index);

          clock_t st = clock ();
          if (ofoa.sat (in) != ans[0] == 's')
            {
              printf ("\nFail: %s\n", in);
              break;
            }
          double t = ((double) clock () - st) / CLOCKS_PER_SEC;
          if (t > time)
            {
              printf ("\nSlow: %s\nBefore: %.9f\nNow: %.9f\n", in, time, t);
              break;
            }
          printf ("\t%.6f\t%.6f\tpassed\n", time, t, index);

        }
      fclose (fin);
      fclose (fans);
    }
}

void
test_olg ()
{
  freopen("test_data/in", "r",stdin);
  while (gets (in))
    {
      if (in[strlen (in) - 1] == '\n')
        in[strlen (in) - 1] = '\0';
      if (strlen (in) == 0)
        continue;

      double st = currentTime ();
      aalta::aalta_formula af (in);
      printf ("in: %s\n", in);
      printf ("af: %s\n", af.to_string ().c_str ());
      aalta::olg_formula olg (&af);
      printf ("olg_formula: %s\n", olg.to_string ().c_str ());
      printf ("olg_string: %s\n", olg.to_olg_string ().c_str ());
      printf ("time :  %.9fs\n-------\n", currentTime () - st);
    }
  fclose (stdin);
}
*/

void printUsage()
{
  aalta::Printer::printMessage("Usage: ./aalta [para] [formula]");
  aalta::Printer::printMessage("para: ");
  aalta::Printer::printMessage("	-e: print the evidence for satisfiable formula");
  aalta::Printer::printMessage("	-h: print help information");
  aalta::Printer::printMessage("formula:");
  printInput();
}

int
main (int argc, char** argv)
{
  //  test_af ();
  //  test_af_simply ();
  //  test_dnf ();
  //  test_dnf_next ();
//    test_OFOA ();
  //  test_aalta ();
//    return 0;
//  test_olg();
//  return 0;

  //freopen ("test.in", "r", stdin);
  /*
  gets (in);
  aalta::OFSA aalta;
  printf ("%s\n", aalta.sat (in) ? "sat" : "unsat");
  return 0;
  */

  //edited by Jianwen Li
  
  string input;
  bool evidence = false;
  //input = "(F a) & (G ! a )";
  //goto jump;
  if(argc == 1)
  {
    cout << "please input the formula:" << endl;
    getline(cin, input);
  }
  else
  {
    if(argc == 2)
    {
      if(strcmp(argv[1], "-h") == 0)
      {
        printUsage();
        return 0;
      }
      else{
        if(strcmp(argv[1], "-e") == 0)
        {
          evidence = true;
          cout << "please input the formula:" << endl;
          getline(cin, input);
        }
        else
        {
          input = argv[1];
        }
      }
    }
    else
    if(argc == 3)
    {
      if(strcmp(argv[1], "-e") == 0)
      {
        evidence = true;
        input = argv[2];
      }
      else
      {
        printUsage();
        return 0;
      }
    }
    else
    {
      printUsage();
      return 0;
    }
  }
  
//jump:
  //edited by Jianwen Li
  

  /*
  string input;
  ifstream infile("test.in");
  getline(infile, input);
  */
  //gets (input);
  aalta::OFSA ofoa;
  bool ret = ofoa.sat(input);
  if(ret)
  {
    std::cout << "sat" << std::endl;
    if(evidence)
    {
      if(ofoa.get_result().empty())
      {
        std::cout << "Cannot give the evidence right now!" << std::endl;
      }
      else
      	printf("%s\n", ofoa.get_result().c_str());
    }
  }
  else
  {
    std::cout << "unsat" << std::endl;
  }

 // printf ("%s\n", ofoa.sat (input) ? "sat" : "unsat");
//  aalta::OFSA ofoa;
//  printf ("%s\n", ofoa.sat (argv[1]) ? "sat" : "unsat");
  return 0;
}

