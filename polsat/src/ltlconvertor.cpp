#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h> 
#include <errno.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include "printer.hh"
#include "aalta_formula.hh"
#include "public.hh"

using namespace aalta;
using namespace std;

std::set<std::string> ATOMS;

void print_usage()
{
  Printer::printErrorMessage("error!");
  exit(0);
}

tool_t 
set_tool(char* str)
{
  tool_t tool = Aalta;
  if(strcmp(str, "-pltl") == 0)
  {
     tool = pltl;
  }
  else
  {
     if(strcmp(str, "-trp") == 0)
     {
       tool = trppp;
     }
     else
     {
       if(strcmp(str, "-smv") == 0)
       {
         tool = smv;
       }
       else
       {
         if(strcmp(str, "-aalta") == 0)
         {
           tool = Aalta;
         }
         else
         {
           print_usage();
           exit(0);
         }
       }
     }
   }
   return tool;
}



int
main (int argc, char** argv)
{
  tool_t tool = Aalta;
  std::string input;
  if (argc == 1)
    {
      cout << "please input the formula:" << endl;
      getline (cin, input);
      tool = Aalta;
    }
  else
    {
      if(argc == 2)
      {
        tool = set_tool(argv[1]);
        
        
        cout << "please input the formula:" << endl;
        getline (cin, input);
      }
      else
      {
        if(argc > 3)
        {
          print_usage();
          exit(0);
        }
        else
        {
          tool = set_tool(argv[1]);
          input = argv[2];
        }
      }
       
    }
  aalta_formula* formula = aalta::parse(input);
  
  if(formula == NULL)
  {
    Printer::printErrorMessage("formula parse error!\n");
    exit(0);
  }
  std::string result = formula->to_string(tool);
  std::set<std::string>::iterator it;
  if(tool == smv || tool == smv_bmc)
  {
    std::cout << "MODULE main" << std::endl;
    std::cout << "VAR" << std::endl;
    for(it = ATOMS.begin(); it != ATOMS.end(); it ++)
    {
      std::cout << *it << " : boolean;" << std::endl;
    }
    std::cout << "LTLSPEC" << std::endl;
    std::cout << "!( " << result << " )" << std::endl;
  }
  else
  {
    std::cout << result << std::endl;
  }
  return (0);
}
