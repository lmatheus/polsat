#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h> 
#include <errno.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include <sys/time.h>

#include "printer.hh"
#include "aalta_formula.hh"
#include "public.hh"
#include "utility.hh"

using namespace aalta;
using namespace std;

#define P_SIZE 3
#define MAX_SIZE 10000

#define PLTLIN "../pltl/tmpin.pltl"
#define PLTLOUT "../pltl/tmpout.pltl"
#define SMVIN "../NuSMV/tmpin.smv"
#define SMVOUT "../NuSMV/tmpout.smv"
#define SMVBMCIN "../NuSMV/tmpin.smvbmc"
#define SMVBMCOUT "../NuSMV/tmpout.smvbmc"
#define TRPIN "../trp++/tmpin.trp"
#define TRPOUT "../trp++/tmpout.trp"
#define TRPIN2 "../trp++/tmpin2.trp"
#define AALTAIN "../Aalta/tmpin.aalta"
#define AALTAOUT "../Aalta/tmpout.aalta"

#define PLTLPATH "../pltl/pltl"
#define SMVPATH "../NuSMV/nusmv/NuSMV"
#define TRPPATH "../trp++/bin/trp++"
#define AALTAPATH "../Aalta/src/aalta"
#define TRPTRANPATH "../trp++/bin/translate.bin"

#define EXIN "../extool.in"
#define EXOUT "../extool.out"



pthread_t pids[P_SIZE];
pthread_t pid;

string input;
aalta_formula *formula;
std::set<std::string> ATOMS;
string output;
std::string multcases;
std::string extool;

double
currentTime ()
{
  struct timeval tv;
  gettimeofday (&tv, NULL);
  return tv.tv_sec + tv.tv_usec / 1000000.0;
}

void
kill_others ()
{
 
  for (int i = 0; i < P_SIZE; ++i)
    pthread_cancel (pids[i]);
  
  if(!extool.empty())  
    pthread_cancel (pid);
    
}

std::string 
replaceChar(std::string str, char ch1, char ch2)
{
  std::string result;
  for (int i = 0; i < str.length(); ++i) 
  {
    if (str[i] == ch1)
      result[i] = ch2;
    else
    {
      result[i] = str[i];
    }
  }
  return result;
}

//input must be obtained from regulation
void
converInputTo(tool_t tool, std::string line)
{
  std::string str;
  if(!line.empty())
  {
    //cout << "line is: " << line << endl;
    ATOMS.clear();
    aalta_formula* f = aalta::parse(line);
    str = f->to_string(tool);
    //cout << "str is: " << str << endl;
  }
  else
  {
    //cout << "line is empty." << endl;
    str = formula->to_string(tool);
  }
  
  ofstream myfile;
  std::set<std::string>::iterator it;
  //str = formula->to_string(tool);
  switch (tool)
    {
    case pltl: 
      myfile.open (PLTLIN);
      myfile << str;
      myfile.close();
      break;
    case smv:
      myfile.open (SMVIN);
      myfile << "MODULE main\n";
      myfile << "VAR\n";
     // std::cout << "smv before using : " << ATOMS.size() << std::endl;
      for(it = ATOMS.begin(); it != ATOMS.end(); it ++)
      {
        myfile << *it << ":boolean;\n";
      }
      myfile << "LTLSPEC\n";
      myfile << "!(" << str << ")\n";
      myfile.close();
      break;
    case smv_bmc:
      myfile.open (SMVBMCIN);
      myfile << "MODULE main\n";
      myfile << "VAR\n";
      //std::cout << "smvbmc before using : " << ATOMS.size() << std::endl;
      for(it = ATOMS.begin(); it != ATOMS.end(); it ++)
      {
        myfile << *it << ":boolean;\n";
      }
      myfile << "LTLSPEC\n";
      myfile << "!(" << str << ")\n";
      myfile.close();
      break;
    case trppp:
      myfile.open (TRPIN);
      myfile << str;
      myfile.close();
      break;
    case Aalta:
      myfile.open (AALTAIN);
      myfile << str;
      myfile.close();
      break;
    default:
      Printer::printErrorMessage("Cannot recognize the tool type !\n");
      exit(0);
    }
}

void run_tool(tool_t tool)
{
  std::string command;
  switch(tool)
  {
    case pltl:
    {
      command = std::string("cat ") + PLTLIN + " | " + PLTLPATH + " graph >" + PLTLOUT;
      break;
    }
    case smv:
    {
      command = std::string(SMVPATH) + " " + SMVIN + " > " + SMVOUT;
      break;
    }
    case smv_bmc:
    {
      command = std::string(SMVPATH) + " -bmc -bmc_length 200 " + SMVBMCIN + " > " + SMVBMCOUT;
      break;
    }
    case trppp:
    {
      command = "";
      break;
    }
    case Aalta:
    {
      command = std::string("cat ") + AALTAIN + " | " + AALTAPATH + " > " + AALTAOUT;
      break;
    }
    default:
    {
      Printer::printErrorMessage("Cannot recognize the tool type !\n");
      exit(0);
    }
  }
  if(tool != trppp)
  {
    system(command.c_str());
  }
  else
  {
    command = std::string(TRPTRANPATH) + " -i " + TRPIN + " -o " + TRPIN2;
    system(command.c_str());
    command = std::string(TRPPATH) + " " + TRPIN2 + " > " + TRPOUT;
    system(command.c_str());
  }
}

std::string 
get_tool(std::string str)
{
  std::string tools[4];
  tools[0] = "pltl";
  tools[1] = "smvbmc";
  tools[2] = "smv";
  tools[3] = "aalta";
  tools[4] = "trp";
  for(int i = 0; i < 5; i ++)
  {
    if(str.find(tools[i]) != std::string::npos)
    {
      return tools[i];
    }
  }
  return "";
}

std::string 
get_output(tool_t tool)
{
  std::string result;
  //ifstream in;
  std::string SAT, UNSAT;
  std::string filename;
  std::string line;
  
  switch(tool)
  {
    case pltl:
    {
      filename = PLTLOUT;
      SAT = "satisfiable";
      UNSAT = "unsatisfiable";
      
      break;
    }
    case smv:
    {
      filename = SMVOUT;
      SAT = "is false";
      UNSAT = "is true";
      break;
    }
    case smv_bmc:
    {
      filename = SMVBMCOUT;;
      SAT = "is false";
      UNSAT = "is true";
      break;
    }
    case trppp:
    {
      filename = TRPOUT;;
      SAT = "Satisfiable";
      UNSAT = "Unsatisfiable";
      break;
    }
    case Aalta:
    {
      filename = AALTAOUT;;
      SAT = "sat";
      UNSAT = "unsat";
      break;
    }
    default:
    {
      Printer::printErrorMessage("Cannot recognize the tool type !\n");
      exit(0);
    } 
  }
  FILE* fp = fopen(filename.c_str(),"r");
  if(fp == NULL)
  {
    Printer::printErrorMessage("Cannot open the file: " + filename + " !\n");
    exit(0);
  }
  char str[MAX_SIZE];
  bool flag = false;
  while(fgets(str, MAX_SIZE, fp))
  {
    line = str;
    if(line.find(UNSAT) != std::string::npos)
    {
      result = "unsat";
      flag = true;
    }
    else
    {
      if(line.find(SAT) != std::string::npos)
      {
        result = "sat";
        flag = true;
      }
    }
  }
  if(!flag)
  {
    Printer::printErrorMessage("Cannot recognize the output from: " + filename + "\n");
    exit(0);
  }
  fclose(fp);
  std::string tname = get_tool(filename);
  result += "\nfrom " + tname;
  return result;
}

std::string invoke_sat(tool_t tool)
{
  converInputTo(tool, "");
  run_tool(tool);
  std::string output = get_output(tool);
  return output;
}

void invoke(tool_t tool, std::string line)
{
  converInputTo(tool, line);
  run_tool(tool);
  std::string output = get_output(tool);
}

void*
thread (void* ptr)
{ 
  
  tool_t tool = *((tool_t*) ptr);
  //cout << "tool: " << tool << endl;
  output = invoke_sat(tool);
  
  //cout << "tool: " << tool << "\n" << output << endl;
  kill_others ();
}

std::string 
out(std::string output)
{
  std::string result = "";
  if(output.find("pltl") != std::string::npos)
  {
    result += "pltl: ";
  }
  else
  {
    if(output.find("aalta") != std::string::npos)
    {
      result += "aalta: ";
    }
    else
    {
      if(output.find("smvbmc") != std::string::npos)
      {
        result += "smvbmc: ";
      }
      else
      {
        if(output.find("smv") != std::string::npos)
        {
          result += "smv: ";
        }
        else
        {
          if(output.find("trp") != std::string::npos)
          {
            result += "trp: ";
          }
          else
          {
            std::cout << "unrecognized output!" << std::endl;
            exit(0);
          }
        }
      }
    }
  }
  
  if(output.find("unsat") != std::string::npos)
  {
    result += "unsat  ";
  }
  else
  {
    if(output.find("sat") != std::string::npos)
    {
      result += "sat  ";
    }
    else
    {
      std::cout << "unrecognized output!" << std::endl;
      exit(0);
    }
  }
  
  return result;
}

void*
thread_separate(void *ptr)
{
  double st = currentTime();
  tool_t tool = *((tool_t*) ptr);
  //cout << "tool: " << tool << endl;
  output = invoke_sat(tool);
  std::string str = out(output);
  //std::cout << output << std::endl;
  std::cout << str << currentTime() - st << "s" <<  std::endl;
}

void*
thread_separate_file(void* ptr)
{
  double st = currentTime();
  tool_t tool = *((tool_t*) ptr);
  //cout << "tool: " << tool << endl;
  std::string line;
  ifstream tmp (multcases.c_str());
  if (tmp.is_open())
  {
    while ( getline (tmp,line) )
    {
     // cout << "line: " << line << endl;
      
      invoke(tool, line);
      //sleep(10);
    }
    tmp.close();
  }
  else 
  {
    cout << "Unable to open file" << endl;
    exit(0);
  }
  
  //std::cout << output << std::endl;
  
  std::string t= "";
  
  switch(tool)
  {
    case pltl:
      t = "pltl"; break;
    case smv:
      t = "NuSMV-BDD"; break;
    case smv_bmc:
      t = "NuSMV-BMC"; break;
    case trppp:
      t = "trp++"; break;
    case Aalta:
      t = "aalta"; break;
  }
  
  cout << t << ": " << currentTime() - st << "s" <<  endl;
  
}

void*
thread_add(void*ptr)
{
  tool_t tool = *((tool_t*) ptr);
  //cout << "tool: " << tool << endl;
  std::string t = std::string((char*) ptr);
  
  std::string s = formula->to_string(Aalta);
  ofstream in;
  in.open (EXIN);
  in << s;
  in.close();
  
  std::string command = std::string("cat ") + EXIN + " | " + t + " >" + EXOUT;
  
  system(command.c_str());
  
  std::string line, result;
  FILE* fp = fopen(EXOUT,"r");
  if(fp == NULL)
  {
    Printer::printErrorMessage("Cannot open the file: " + std::string(EXOUT) + " !\n");
    exit(0);
  }
  char str[MAX_SIZE];
  bool flag = false;
  while(fgets(str, MAX_SIZE, fp))
  {
    line = str;
    if(line.find("unsat") != std::string::npos)
    {
      result = "unsat";
      flag = true;
    }
    else
    {
      if(line.find("sat") != std::string::npos)
      {
        result = "sat";
        flag = true;
      }
    }
  }
  if(!flag)
  {
    Printer::printErrorMessage("Cannot recognize the output from: " + std::string(EXOUT) + "\n");
    exit(0);
  }
  fclose(fp);
  
  output = result + "\n";
  output += "from " + extool;
  
  //cout << "tool: " << tool << "\n" << output << endl;
  kill_others ();
}

void *
thread_separate_add(void* ptr)
{
  double st = currentTime();
  std::string t = std::string((char*) ptr);
  
  std::string s = formula->to_string(Aalta);
  ofstream in;
  in.open (EXIN);
  in << s;
  in.close();
  
  std::string command = std::string("cat ") + EXIN + " | " + t + " >" + EXOUT;
  
  system(command.c_str());
  
  std::string line, result;
  FILE* fp = fopen(EXOUT,"r");
  if(fp == NULL)
  {
    Printer::printErrorMessage("Cannot open the file: " + std::string(EXOUT) + " !\n");
    exit(0);
  }
  char str[MAX_SIZE];
  bool flag = false;
  while(fgets(str, MAX_SIZE, fp))
  {
    line = str;
    if(line.find("unsat") != std::string::npos)
    {
      result = "unsat";
      flag = true;
    }
    else
    {
      if(line.find("sat") != std::string::npos)
      {
        result = "sat";
        flag = true;
      }
    }
  }
  if(!flag)
  {
    Printer::printErrorMessage("Cannot recognize the output from: " + std::string(EXOUT) + "\n");
    exit(0);
  }
  fclose(fp);
  
  std::cout << t << ": " << result << " " << currentTime() - st << "s" <<  std::endl;
}

/*
bool
find_in_file (const string& filename, const string& keyword)
{

  ifstream in (filename.c_str ());
  if (in)
    {
      ostringstream os;
      os << in.rdbuf ();
      string content (os.str ());
      if (string::npos != content.find (keyword))
        return true;
      else
        return false;
    }
  return false;
}
*/

bool flag[P_SIZE];

void init()
{
  for(int i = 0; i < P_SIZE; i ++)
  {
    flag[i] = false;
  }
}

void load_solvers()
{
  struct stat buffer;
  int count = 0;
  if(!extool.empty())
  {
    if(stat (extool.c_str(), &buffer) != 0)
    {
      std::cout << "cannot find " << extool << ", ignored." << std::endl;
      extool = "";
    }
  }
  
  if(stat (PLTLPATH, &buffer) != 0)
  {
    std::cout << "cannot find " << PLTLPATH << ", ignored." << std::endl;
    count ++;
    flag[0] = true;
  }
  
  if(stat (TRPPATH, &buffer) != 0)
  {
    std::cout << "cannot find " << TRPPATH << ", ignored." << std::endl;
    count ++;
    flag[4] = true;
  }
  
  if(stat (SMVPATH, &buffer) != 0)
  {
    std::cout << "cannot find " << SMVPATH << ", ignored." << std::endl;
    count ++;
    flag[1] = true;
    flag[2] = true;
  }
  
  if(stat (AALTAPATH, &buffer) != 0)
  {
    std::cout << "cannot find " << AALTAPATH << ", ignored." << std::endl;
    count ++;
    flag[3] = true;
  }
  
  if(count == P_SIZE - 1)
  {
    std::cout << "There must be at least one avaiable solver!" << std::endl;
    exit(0);
  }
}

void kill_solvers()
{
  pid_t pid = proc_find("pltl");
  if(pid < 0)
  {
  }
  else
  {
    system("killall -9 pltl 2>/dev/null");
  }
  
  pid = proc_find("NuSMV");
  if(pid < 0)
  {
  }
  else
  {
    system("killall -9 NuSMV 2>/dev/null");
  }
  
  
  pid = proc_find("aalta");
  if(pid < 0)
  {
  }
  else
  {
    system("killall -9 aalta 2>/dev/null");
  }
  
  //To be done, remove the added solver
  
  
}

void remove_tmpfiles()
{
  std::string str[8];
  str[0] = AALTAIN;
  str[1] = AALTAOUT;
  str[2] = PLTLIN;
  str[3] = PLTLOUT;
  str[4] = SMVIN;
  str[5] = SMVOUT;
  str[6] = SMVBMCIN;
  str[7] = SMVBMCOUT;
  std::string cmd;
  for(int i = 0; i < 8; i ++)
  {
    cmd = "rm " + str[i] + " 2>/dev/null";
    system(cmd.c_str());
  }

}

void printUsage()
{
  aalta::Printer::printMessage("Usage: ./polsat [para]");
  aalta::Printer::printMessage("para: ");
  aalta::Printer::printMessage("	-e: print the evidence for satisfiable formula");
  aalta::Printer::printMessage("	-s: run all integrated solvers separately");
  aalta::Printer::printMessage("	-sm f: run all integrated solvers separately, and get the inputs from file 'f'");
  aalta::Printer::printMessage("	-add path: add a external solver which is identified by the parameter 'path'");
  aalta::Printer::printMessage("	-h: print help information");
}

void invoke_aalta_evi()
{  
  double st = currentTime();
  std::string str = formula->to_string(Aalta);
  std::string command = std::string(AALTAPATH) + " -e " + "\"" + str + "\"";
  system(command.c_str());
  cout << "Eciplse time: " << currentTime() - st << "s" <<  endl;
}

void run_tools_file()
{
  /*
  tool_t tool = Aalta;
  thread_separate_file((void*) &tool);
  */
 
  int i, ret;
  
    tool_t tools[P_SIZE];
    for (i = 0; i < P_SIZE; ++i)
    {
      if(flag[i])
        continue;
      tools[i] = (tool_t) i;
      ret = pthread_create (&pids[i], NULL, thread_separate_file, (void*) &tools[i]);
      //To be done !!
      sleep(1);
      if (ret != 0)
        {
          printf ("Create pthread error!\n");
          exit (1);
        }
    }
    
    if(!extool.empty())
    {
      ret = pthread_create(&pid, NULL, thread_separate_add, (void*) &extool);
      if (ret != 0)
        {
          printf ("Create pthread error!\n");
          exit (1);
        }
    }
    //cout <<"time of creating thread: " << currentTime() - st << "s" << endl;

    
    for (i = 0; i < P_SIZE; ++i)
    {
      if(flag[i])
        continue;
      pthread_join (pids[i], NULL);
    }

    if(!extool.empty())
      pthread_join(pid, NULL);

    remove_tmpfiles();
    
}

void run_tools(bool b)
{

  int i, ret;
  tool_t tools[P_SIZE];

  if(b)
  {
    
    for (i = 0; i < P_SIZE; ++i)
    {
      if(flag[i])
        continue;
      tools[i] = (tool_t) i;
      ret = pthread_create (&pids[i], NULL, thread_separate, (void*) &tools[i]);
      if (ret != 0)
        {
          printf ("Create pthread error!\n");
          exit (1);
        }
    }
    if(!extool.empty())
    {
      ret = pthread_create(&pid, NULL, thread_separate_add, (void*) &extool);
      if (ret != 0)
        {
          printf ("Create pthread error!\n");
          exit (1);
        }
    }
    //cout <<"time of creating thread: " << currentTime() - st << "s" << endl;

    
    for (i = 0; i < P_SIZE; ++i)
    {
      if(flag[i])
        continue;
      pthread_join (pids[i], NULL);
    }
    if(!extool.empty())    
      pthread_join(pid, NULL);
    remove_tmpfiles();
  }
  else
  {
    //cout << "time of parsing: " << currentTime() - st  << "s"<< endl;
    //std::cout << formula->to_string(tool) << std::endl;
  
    //std::cout << "after parsing, ATOMS size: " << ATOMS.size() << std::endl;
    double st = currentTime();
  
    for (i = 0; i < P_SIZE; ++i)
    {
      if(flag[i])
        continue;
      tools[i] = (tool_t) i;
      ret = pthread_create (&pids[i], NULL, thread, (void*) &tools[i]);
      if (ret != 0)
        {
          printf ("Create pthread error!\n");
          exit (1);
        }
    }
    if(!extool.empty())
    {
      cout << "create extool: " << extool << endl;
      ret = pthread_create(&pid, NULL, thread_add, (void*) &extool);
      if (ret != 0)
        {
          printf ("Create pthread error!\n");
          exit (1);
        }
    }
    //cout <<"time of creating thread: " << currentTime() - st << "s" << endl;

    st = currentTime();
    for (i = 0; i < P_SIZE; ++i)
    {
      if(flag[i])
        continue;
      pthread_join (pids[i], NULL);
    }
    if(!extool.empty())
      pthread_join(pid, NULL);

    //cout << "end" << endl;
    cout << output << endl;
    cout << "Eciplse time: " << currentTime() - st << "s" <<  endl;

    kill_solvers();
    remove_tmpfiles();
  }
  
}

int
main (int argc, char** argv)
{
  bool evidence = false;
  bool separate = false;
  bool sep_mult = false;
  bool add = false;
  
  //std::string extool;
  extool = "";
  input = "";
  if (argc == 1)
    {
      cout << "please input the formula:" << endl;
      getline (cin, input);
    }
  else
    {
      for(int i = 1; i < argc; i ++)
      {
        if(strcmp(argv[i], "-h") == 0)
        {
          printUsage();
          return 0;
        }
        else
        if(strcmp(argv[i], "-e") == 0)
        {
          evidence = true;
          //cout << "please input the formula:" << endl;
          //getline(cin, input);
        }
        else
        if(strcmp(argv[i], "-s") == 0)
        {
          separate = true;
          //cout << "please input the formula:" << endl;
          //getline(cin, input);
        }
        else
        if(strcmp(argv[i], "-sm") == 0)
        {
          sep_mult = true;
          i ++;
          if(i >= argc)
          {
            printUsage();
            return 0;
          }
          multcases = argv[i];
        }
        else
        if(strcmp(argv[i], "-add") == 0)
        {
          add = true;
          i ++;
          if(i >= argc)
          {
            printUsage();
            return 0;
          }
          extool = argv[i];
        }
        else
        {
          input = argv[i];
        }
      }
    }
    if(input.empty() && !sep_mult)
    {
      cout << "please input the formula:" << endl;
      getline(cin, input);
    }
    
  //double st = currentTime();
  
  
  
  init();

  load_solvers();
  
  if(!sep_mult)
  {
    formula = aalta::parse(input);
  
    if(formula == NULL)
    {
      Printer::printErrorMessage("formula parse error!\n");
      return 0;
    }
  }
  
  if(evidence)
  {
    if(separate)
    {
      Printer::printErrorMessage("The parameters -e and -s cannot be used togeghter!");
      return 0;
    }
    invoke_aalta_evi();
    return 0;
  }
  
  
  if(separate)
  {
    run_tools(true);
    return 0;
  }
  
  if(sep_mult)
  {
    run_tools_file();
    return 0;
  }

  run_tools(false);
  
  
  
  return (0);
}
