/* 
 * File:   printer.hh
 * Author: snowingsea
 *
 * 打印类
 * Created on July 17, 2013, 9:36 PM
 */

#ifndef PRINTER_HH
#define	PRINTER_HH

#include <stdio.h>
#include <string>

  namespace aalta
  {

    class Printer
    {
    public:

      static void
      printMessage (const std::string& msg)
      {
        printf ("%s\n", msg.c_str ());
        //        Logger::getInstance().log(text);
      }

      static void
      printErrorMessage (const std::string& msg)
      {
        printf ("%s\n", msg.c_str ());
      }
    };
  }



#endif	/* PRINTER_HH */

