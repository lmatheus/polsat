

#ifndef AALTA_PUBLIC_HH
# define AALTA_PUBLIC_HH

//#include "aalta_formula.hh"
#include <set>

#include <iostream>

namespace aalta
{
  class aalta_formula;
  aalta_formula* parse(const std::string&);
}

enum tool_t
{
  smv, smv_bmc, Aalta, pltl, trppp
};

inline void printInput()
{
  std::cout << "Aalta support the following symbols:" << std::endl;
  std::cout << "	not:		!, ~" << std::endl;
  std::cout << "	and:		&, &&" << std::endl;
  std::cout << "	or:		|, ||" << std::endl;
  std::cout << "	next:		X" << std::endl;
  std::cout << "	until:		U" << std::endl;
  std::cout << "	release:	R, V" << std::endl;
  std::cout << "	global:		G, []" << std::endl;
  std::cout << "	future:		F, <>" << std::endl;
  std::cout << "	imply:		->" << std::endl;
  std::cout << "	equiv:		<->" << std::endl;
  std::cout << "	true:		true, True" << std::endl;
  std::cout << "	false:		false, False" << std::endl;
  std::cout << "If you are not sure the precedence among operators, please use ()." << std::endl;
  std::cout << std::endl;
  std::cout << "For more information, please use the -h flag.\n" << std::endl;
}

extern std::set<std::string> ATOMS;


#endif // AALTA_PUBLIC_HH
