/* 
 * 常用函数类
 * File:   utility.hh
 * Author: snowingsea
 *
 * Created on July 18, 2013, 4:01 PM
 */

#ifndef UTILITY_HH
#define	UTILITY_HH

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <math.h>
#include <time.h>
#include <cstdlib>
#include <sys/types.h>
#include <dirent.h>
#include<unistd.h>
#include<string.h>

#define eps 1e-7
#define HASH_INIT 1315423911

/**
 * 绝对值
 * @param d
 * @return 
 */
inline double fabs(double d) {
    return d >= 0 ? d : -d;
}

/**
 * 判double量是否为0
 * @param d
 * @return 
 */
inline bool _zero(double d) {
    return d < eps && d > -eps;

}

/**
 * 判两个double是否相等
 * @param a
 * @param b
 * @return 
 */
inline bool equal(double a, double b) {
    return _zero(a - b);
}

/**
 * 转string
 * @param val
 * @return 
 */
template <typename T>
std::string convertToString(T val) {
    std::stringstream ss;
    ss << val;
    return ss.str();
}

/**
 * 交换两个变量值
 * @param a
 * @param b
 */
template <typename T>
inline void _swap(T& a, T& b) {
    T tmp = a;
    a = b;
    b = tmp;
}

/**
 * 获取两个变量的最小值
 * @param a
 * @param b
 * @return 
 */
template <typename T>
inline T _min(const T& a, const T& b) {
    return a < b ? a : b;
}

template <typename T>
inline T _min(std::vector<T>& vec) {
    if (vec.empty()) return NULL;
    T m = vec[0];
    for (int i = 1; i < vec.size(); ++i) {
        if (vec[i] < m)
            m = vec[i];
    }
    return m;
}

template <typename T>
inline T _max(std::vector<T>& vec) {
    if (vec.empty()) return NULL;
    T m = vec[0];
    for (int i = 1; i < vec.size(); ++i) {
        if (vec[i] > m)
            m = vec[i];
    }
    return m;
}

template <typename T>
inline T _sum(std::vector<T>& vec) {
    if (vec.empty()) return NULL;
    T m = vec[0];
    for (int i = 1; i < vec.size(); ++i) {
        m += vec[i];
    }
    return m;
}

/**
 * 获取两个变量的最大值
 * @param a
 * @param b
 * @return 
 */
template <typename T>
inline T _max(const T& a, const T& b) {
    return a > b ? a : b;
}

/**
 * 判double是否为整数
 * @param d
 * @return 
 */
inline bool isInt(double d) {
    return _zero(d - round(d));
}

/**
 * 获取[st,ed]之间的随机数
 * @param st
 * @param ed
 * @return 
 */
static bool have_set_srand = false;

inline int randInt(int st, int ed) {
    if (ed < st) return st;
    if (!have_set_srand) {
        srand((unsigned) time(0));
        have_set_srand = true;
    }
    return rand() % (ed - st + 1) + st;
}

/**
 * 随机[0,1]浮点数
 * @return 
 */
#define RANDMAX 1000

inline double randFloat() {
    return randInt(0, RANDMAX) / (double) RANDMAX;
}

inline double randFloat(double st, double ed) {
    if (ed <= st) return st;
    return randFloat() * (ed - st) + st;
}

inline std::string randColor() {
    std::string color = "rgb(";
    color += convertToString(randInt(0, 255));
    color += ",";
    color += convertToString(randInt(0, 255));
    color += ",";
    color += convertToString(randInt(0, 255));
    color += ")";
    return color;
}

/**
 * 计算二进制中1的个数
 * @param n
 * @return 
 */
inline int bitCount(unsigned int n) {
    int c = 0;
    for (; n; ++c)
        n &= (n - 1); //清除最低位的1
    return c;
}

/**
 * 计算二进制整数长度
 * @param n
 * @return 
 */
inline int bitLength(unsigned int n) {
    int c = 0;
    for (; n; ++c)
        n >>= 1;
    return c;
}

/**
 * 获取二进制第p位的数字,自右0起
 * @param n
 * @param p
 * @return 
 */
inline int bit(int n, int p) {
    return (n >> p)&1;
}

/**
 * 逻辑斯蒂S形函数
 * @param a
 * @param p 控制曲线形状变化快慢或陡峭性.当p赋以较大值时，曲线就显得平坦，反之，就会使曲线变为陡峭
 * @return 
 */
inline double sigmoid(double a, double p = 1) {
    return 1 / (1 + exp(-a / p));
}

inline double sqr(double x) {
    return x*x;
}

template<typename T>
inline void print(T msg) {
    printf("%s", convertToString(msg).c_str());
}

inline int roulette_wheel_select(std::vector<double>& v) {
    int i;
    double slice = 0;
    for (i = 0; i < v.size(); ++i)
        slice += v[i];
    slice = randFloat() * slice;
    for (i = 0; i < v.size(); ++i) {
        slice -= v[i];
        if (slice <= 0)
            return i;
    }
    return v.size() - 1;
}


inline bool file_write(const char* path, const char* str){
  FILE* fp = fopen(path, "w");
  fprintf(fp, "%s", str);
  fclose(fp);
}

/**
 find the process id according to the process name;
 */
inline pid_t proc_find(const char* name) 
{
    DIR* dir;
    struct dirent* ent;
    char buf[512];

    long  pid;
    char pname[100] = {0,};
    char state;
    FILE *fp=NULL; 

    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return -1;
    }

    while((ent = readdir(dir)) != NULL) {
        long lpid = atol(ent->d_name);
        if(lpid < 0)
            continue;
        snprintf(buf, sizeof(buf), "/proc/%ld/stat", lpid);
        fp = fopen(buf, "r");

        if (fp) {
            if ( (fscanf(fp, "%ld (%[^)]) %c", &pid, pname, &state)) != 3 ){
                printf("fscanf failed \n");
                fclose(fp);
                closedir(dir);
                return -1; 
            }
            if (!strcmp(pname, name)) {
                fclose(fp);
                closedir(dir);
                return (pid_t)lpid;
            }
            fclose(fp);
        }
    }


closedir(dir);
return -1;
}


#endif	/* UTILITY_HH */

