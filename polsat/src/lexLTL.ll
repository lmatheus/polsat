%{


/* This is a lexer for LTL formulas in in-fix order */

#include "yaccLTL.tab.hh"
#include "parsedecl.hh"
#include <string.h>
#include "public.hh"
//#include "aalta_formula.hh"

//using namespace aalta;
using namespace yy;




%}
%%
[\t ]+       /*ignore whitespace*/ ;

&&        { 
  /*ECHO; /* normal default anyway */ 
    return parser::token::AND;
}
&  { 
    /*fprintf(stderr, "%s is a logical operator\n", yytext);*/
    return parser::token::AND;
   }
\|\|        { 
  /*ECHO; /* normal default anyway */ 
    return parser::token::OR;
}
\| { 
    /*fprintf(stderr, "%s is a logical operator\n", yytext);*/
    return parser::token::OR;
   }

~  { 
    /*fprintf(stderr, "%s is a logical operator\n", yytext);*/
    return parser::token::NOT;
   }
!  { 
    /*fprintf(stderr, "%s is a logical operator\n", yytext);*/
    return parser::token::NOT;
   }
-> { 
    /*fprintf(stderr, "%s is a logical operator\n", yytext);*/
    return parser::token::IMPLIES;
   }
\<-> {
    return parser::token::EQUIV;
    }
X  { 
    /*fprintf(stderr, "%s is a temporal operator\n", yytext);*/
    return parser::token::NEXT;
   }
U  { 
    /*fprintf(stderr, "%s is a temporal operator\n", yytext);*/
    return parser::token::UNTIL;
   }
R  { 
    /*fprintf(stderr, "%s is a temporal operator\n", yytext);*/
    return parser::token::RELEASE;
   }
V  {
    return parser::token::RELEASE;
   }
G  { 
    /*fprintf(stderr, "%s is a temporal operator\n", yytext);*/
    return parser::token::GLOBALLY;
   }
\[\] {
    return parser::token::GLOBALLY;
   }
F  { 
    /*fprintf(stderr, "%s is a temporal operator\n", yytext);*/
    return parser::token::FUTURE;
   }
\<> {
    return parser::token::FUTURE;
   }
FALSE  { 
    /*fprintf(stderr, "%s is a boolean truth value\n", yytext);*/
    return parser::token::FFALSE;
   }
false {
    return parser::token::FFALSE;
      }
False {
    return parser::token::FFALSE;
      }
TRUE  { 
  /*fprintf(stderr, "%s is a boolean truth value\n", yytext);*/
    return parser::token::TTRUE;
   }
true  { 
  /*fprintf(stderr, "%s is a boolean truth value\n", yytext);*/
    return parser::token::TTRUE;
   }
True  { 
  /*fprintf(stderr, "%s is a boolean truth value\n", yytext);*/
    return parser::token::TTRUE;
   }

[a-zA-Z_0-9]+   { 
  yylval->varName = (char *)malloc((strlen(yytext)+1)*sizeof(char));
  if (yylval->varName==NULL){ fprintf(stderr, "Memory error24\n"); exit(1); }
  /*copy in the variable name*/
  strcpy(yylval->varName, yytext);
  ATOMS.insert(yytext);
  return parser::token::PROP;
}

\( {
  /*fprintf(stderr, "Got '('\n");*/
  return parser::token::LPAREN;
}

\) {
  /*fprintf(stderr, "Got ')'\n");*/
    return parser::token::RPAREN;
  }

\n { /*This signifies the end of input*/ 
    return 0;}

.        { 
    ECHO; /* normal default anyway */ 
    fprintf(stderr, "ERROR: Unrecognized symbol: %s\n", yytext);
    printInput();
    exit(1);
}
%%

void
flex_set_buffer(const char* buf)
{
  yypush_buffer_state(YY_CURRENT_BUFFER);
  yy_scan_string(buf);
}

void
flex_unset_buffer()
{
  yypop_buffer_state();
}


/*int main(void) {
    yylex();

    return 0;
} /*end main*/
