/* 
 * File:   aalta_formula.cc
 * Author: snowingsea
 * 
 * Created on July 16, 2013, 9:58 AM
 */
#include <stdarg.h>

//#include "ltlparse/ltlparse.hh"

#include "aalta_formula.hh"
#include "public.hh"
#include "printer.hh"
//#include "yaccLTL.tab.hh"
#include <iostream>
#include <algorithm>


  namespace aalta
  {
    ///////////////////////////////////////////
    /* 初始化静态变量 */
    std::vector<std::string> aalta_formula::names;
    std::map<std::string, int> aalta_formula::name_ids;
    aalta_formula::af_prt_map aalta_formula::all_afs;
    aalta_formula::af_list_set aalta_formula::all_list;

    /**
     * 生成位置标识为tag的TRUE公式
     * @param tag
     * @return 
     */
    aalta_formula*
    aalta_formula::TRUE (tag_t tag)
    {
      return aalta_formula (True, NULL, NULL, tag).unique ();
    }

    /**
     * 生成位置标识为tag的FALSE公式
     * @param tag
     * @return 
     */
    aalta_formula*
    aalta_formula::FALSE (tag_t tag)
    {
      return aalta_formula (False, NULL, NULL, tag).unique ();
    }

    /**
     * 初始化静态变量
     */
    void
    aalta_formula::init_static ()
    {
      if (names.empty ())
        {
          //按顺序
          names.push_back ("True");
          names.push_back ("False");
          names.push_back ("Literal");
          names.push_back ("!");
          names.push_back ("|");
          names.push_back ("&");
          names.push_back ("X");
          names.push_back ("U");
          names.push_back ("R");
          names.push_back ("G");
          names.push_back ("F");
          names.push_back ("->");
          names.push_back ("<->");
          names.push_back ("Undefined");
        }
    }

    /**
     * 获取符号对应名称或变量id对应的变量名
     * @param index
     * @return 
     */
    std::string
    aalta_formula::get_name (int index)
    {
      return names[index];
    }

    /**
     * 销毁静态变量, 释放资源
     */
    void
    aalta_formula::destroy ()
    {
      for (af_prt_map::iterator it = all_afs.begin (); it != all_afs.end (); it++)
        delete it->second;
      all_afs.clear ();
      for(af_list_set::iterator it = all_list.begin(); it != all_list.end(); it++)
        delete (*it);
      all_list.clear();
    }

    /**
     * 判断af是否与集合中的元素互斥, 即是否存在 a 和 !a
     * @param af
     * @param pos
     * @param neg
     * @return 
     */
    bool
    aalta_formula::mutex (aalta_formula* af, af_id_set& pos, af_id_set& neg)
    {
      if (af == NULL) return false;
      switch (af->op)
        {
        case False:
          return true;
        case Not:
          if (af->right->op <= Undefined) return false;
          if (pos.find (af->right->op) != pos.end ())
            return true;
          neg.insert (af->right->op);
          break;
        default:
          if (af->op <= Undefined) return false;
          if (neg.find (af->op) != neg.end ())
            return true;
          pos.insert (af->op);
          break;
        }
      return false;
    }

    /**
     * 将链表结构转化为aalta_formula的与或结构
     * @param op
     * @param af_list
     * @return 
     */
    aalta_formula*
    aalta_formula::list2af (int op, std::list<aalta_formula*>& af_list, tag_t tag)
    {
      if (op != And && op != Or)
        {
          Printer::printErrorMessage ("can not convert from list to aalta_formula!");
          return NULL;
        }
      int size = af_list.size ();
      switch (size)
        {
        case 0:
          if (op == And)
            return aalta_formula::TRUE (tag);
          else
            return aalta_formula::FALSE (tag);
        case 1:
          return af_list.front ()->unique ();
        default:
          {
            aalta_formula* l_afp = af_list.front ();
            af_list.pop_front ();
            aalta_formula* r_afp = af_list.back ();
            af_list.pop_back ();
            for (; !af_list.empty (); af_list.pop_back ())
              r_afp = aalta_formula (op, af_list.back (), r_afp, tag).unique ();
            return aalta_formula (op, l_afp, r_afp, tag).unique ();
          }
        }
    }

    /**
     * 判两个公式是否冲突
     * @param af1
     * @param af2
     * @return 
     */
    bool
    aalta_formula::is_conflict (aalta_formula* af1, aalta_formula* af2)
    {
      std::list<aalta_formula*> *af_list = NULL;
      switch (af1->op)
        {
        case False:
          return true;
        case Next:
          switch (af2->op)
            {
            case Next:
              return is_conflict (af1->right, af2->right);
            case Until:
            case Or:
              return is_conflict (af1, af2->left) && is_conflict (af1, af2->right);
            case Release:
              return is_conflict (af1, af2->right);
            case And:
              return is_conflict (af1, af2->left) || is_conflict (af1, af2->right);
            default:
              return false;
            }
        case Until:
          switch (af2->op)
            {
            case Until:
            case Or:
              return is_conflict (af1, af2->left) && is_conflict (af1, af2->right);
            case Release:
              return is_conflict (af1, af2->right);
            case And:
              af_list = new std::list<aalta_formula*>();
              af_list->push_back (af1);
              af2->to_list (*af_list);
              break;
//              return is_conflict (af1, af2->left) || is_conflict (af1, af2->right) || is_conflict (af2->left, af2->right);
            case Next:
            default:
              return is_conflict (af2, af1);
            }
          break;
        case Release:
          switch (af2->op)
            {
            case Or:
              return is_conflict (af1->right, af2->left) && is_conflict (af1->right, af2->right);
            case Release:
              return is_conflict (af1->right, af2->right);
            case And:
              return is_conflict (af1->right, af2->left) || is_conflict (af1->right, af2->right);
            case Until:
            case Next:
            default:
              return is_conflict (af2, af1->right);
            }
        case Or:
          switch (af2->op)
            {
            case Or:
              return is_conflict (af1, af2->left) && is_conflict (af1, af2->right);
            case And:
              return is_conflict (af1, af2->left) || is_conflict (af1, af2->right);
            case Release:
              return is_conflict (af1, af2->right);
            case Until:
            case Next:
            default:
              return is_conflict (af2, af1);
            }
        case And:
          switch (af2->op)
            {
            case And:
              af_list = new std::list<aalta_formula*>();
              af1->to_list (*af_list);
              af2->to_list (*af_list);
              break;
//              return is_conflict (af1, af2->left) || is_conflict (af1, af2->right) || is_conflict (af2->left, af2->right);
            case Release:
              return is_conflict (af1, af2->right);
            case Or:
            case Until:
            case Next:
            default:
              return is_conflict (af2, af1);
            }
          break;
        default:
          switch (af2->op)
            {
            case Next:
              return false;
            case Until:
            case Or:
              return is_conflict (af1, af2->left) && is_conflict (af1, af2->right);
            case Release:
              return is_conflict (af1, af2->right);
            case And:
              af_list = new std::list<aalta_formula*>();
              af_list->push_back (af1);
              af2->to_list (*af_list);
              break;
//              return is_conflict (af1, af2->left) || is_conflict (af1, af2->right) || is_conflict (af2->left, af2->right);
            default:
              return af1->op == Not && af1->right->op == af2->op
                      || af2->op == Not && af2->right->op == af1->op;
            }
          break;
        }
      if (af_list != NULL)
        {
          std::list<aalta_formula*>::iterator it1, it2;
          for (it1 = af_list->begin (); it1 != af_list->end ();)
            {
              aalta_formula* af = *it1;
              for (it2 = ++it1; it2 != af_list->end (); it2++)
                if (is_conflict (af, *it2))
                  return true;
            }
          delete af_list;
        }
      return false;
    }

    /**
     * 获取所有公式信息
     * @return 
     */
    std::string
    aalta_formula::all_to_string ()
    {
      std::string ret = "all_formulas:\n";
      for (af_prt_map::const_iterator it = all_afs.begin (); it != all_afs.end (); it++)
        ret += it->first.to_string () + "[" + convertToString (it->first.tag) + " "
        + convertToString (it->second) + "]" + "\n";
      ret += "name id:\n";
      for (std::map<std::string, int>::const_iterator it = name_ids.begin (); it != name_ids.end (); it++)
        ret += "[" + convertToString (it->second) + "]" + it->first + "\n";
      ret += "----\n";
      return ret;
    }


    ///////////////////////////////////////////

    /**
     * 初始化成员变量
     */
    void
    aalta_formula::init ()
    {
      this->left = this->right = this->unique_prt = NULL;
      this->tag = NULL;
      this->op = Undefined;
      this->length = 0;
      this->init_static ();
    }

    /**
     * 计算hash值
     */
    void
    aalta_formula::clc_hash ()
    {
      hash = HASH_INIT;
      hash = (hash << 5) ^ (hash >> 27) ^ op;
      hash = (hash << 5) ^ (hash >> 27) ^ (size_t) left;
      hash = (hash << 5) ^ (hash >> 27) ^ (size_t) right;
      hash = (hash << 5) ^ (hash >> 27) ^ (size_t) tag;
    }

    aalta_formula::aalta_formula ()
    {
      this->init ();
    }

    aalta_formula::aalta_formula (const aalta_formula& orig)
    {
      *this = orig;
    }

    //modified by Jianwen Li
    aalta_formula::aalta_formula (const std::string& input)
    {
      this->init ();
      aalta_formula* af = parse(input);
      aalta_formula* tmp = input_from (af);
      //std::cout << tmp->to_string() << std::endl;
      this->op = tmp->op;
      this->left = tmp->left == NULL ? NULL : tmp->left->unique ();
      this->right = tmp->right == NULL ? NULL : tmp->right->unique ();
      this->tag = tmp->tag;
      this->unique_prt = tmp->unique_prt;
      this->length = tmp->length;
      //how to delete af ?
      clc_hash ();
    }
    


    aalta_formula::aalta_formula (int op, aalta_formula* left, aalta_formula* right, tag_t tag)
    {
      this->op = op;
      this->left = left == NULL ? NULL : left->unique ();
      this->right = right == NULL ? NULL : right->unique ();
      this->tag = tag;
      this->unique_prt = NULL;
      this->length = 0;
      this->clc_hash ();
    }
    
    std::vector<aalta_formula*> 
    aalta_formula::get_elements(int op_2)
    {
      std::vector<aalta_formula*> result, tmp;
      
      if(op != op_2)
      {
        result.push_back(this);
        return result;
      }
      if(left != NULL)
      {
        tmp = left->get_elements(op_2);
        result.insert(result.end(), tmp.begin(), tmp.end());
      }
      if(right != NULL)
      {
        tmp = right->get_elements(op_2);
        result.insert(result.end(), tmp.begin(), tmp.end());
      }
      return result;
    }
    
    aalta_formula* 
    aalta_formula::input_from(aalta_formula* af)
    {
      aalta_formula result;
      int i, j, size;
      switch(af -> op)
      {
        case True:
        case False:
        {
          result.op = af->op;
          result.left = NULL;
          result.right = NULL;
          break;
        }  
        case Literal:
        {
          name = af->name;
          std::map<std::string, int>::const_iterator it = name_ids.find (name);
          if (it == name_ids.end ())
          { // 此变量名未出现过，添加之
            result.op = names.size ();
            name_ids[name] = result.op;
            names.push_back (name);
          }
          else
          {
            result.op = it->second;
          }
          break;
        }
        case Not:
        case Next:
        case Global:
        case Future:
        {
          result.op = af->op;
          result.left = NULL;
          result.right = input_from(af->right)->unique ();
          break;
        }
        case And:
        case Or:       
        {
          result.op = af->op;
          std::vector<aalta_formula*> fs = af->get_elements(af->op);
          size = fs.size();
          aalta_formula** afp = new aalta_formula*[size];      
          for (i = 0; i < size; ++i)
            afp[i] = input_from(fs[i])->unique ();
          std::sort (afp, afp + size);
          for (i = 0, j = 1; j < size; ++j)
            if (afp[i] != afp[j])
              afp[++i] = afp[j];
          size = i + 1;
          if (size == 1)
            result = *afp[0];
          else
          {
            result.left = afp[0];
            result.right = afp[--size];
            while (--size)
            result.right = aalta_formula (result.op, afp[size], result.right).unique ();
          }
          delete[] afp;
          break;
        }
        case Until:
        case Release: 
        {
          result.op = af->op;
          result.left = input_from(af->left)->unique ();
          result.right = input_from(af->right)->unique ();
          break;
        }      
        case Imply:
        {
          // Implies: a -> b = !a | b
          result.op = Or;
          result.left = aalta_formula (Not, NULL, input_from(af->left)->unique ()).unique ();
          result.right = input_from(af->right)->unique ();
          result = *input_from(result.unique());
          break;
        }
        case Equiv:
        {
          // Equiv: a <-> b = (!a | b) & (!b | a)
          aalta_formula* first = input_from(af->left)->unique ();
          aalta_formula* second = input_from(af->right)->unique ();
          result.op = And;
          result.left = aalta_formula (Or, second,
                                        aalta_formula (Not, NULL, first).unique ()).unique ();
          result.left = input_from(result.left)->unique();
          result.right = aalta_formula (Or, first,
                                         aalta_formula (Not, NULL, second).unique ()).unique ();
          result.right = input_from(result.right)->unique();
          result = *input_from(result.unique())->unique();
          break;
        }
        default:
          if(af->op >= OPNUM)
          {
            result.op = af->op;
          }
          else
          {
            Printer::printErrorMessage ("the formula cannot be recognized by aalta!");
            exit(0);
          }
      }
      return result.unique();
    }

    /**
     * 重载赋值操作符
     * @param af
     * @return 
     */
    aalta_formula& aalta_formula::operator = (const aalta_formula& af)
    {
      if (this != &af)
        {
          this->left = af.left;
          this->right = af.right;
          this->unique_prt = af.unique_prt;
          this->tag = af.tag;
          this->op = af.op;
          this->hash = af.hash;
          this->length = af.length;
        }
      return *this;
    }

    /**
     * 重载等于符号
     * @param af
     * @return 
     */
    bool aalta_formula::operator == (const aalta_formula& af) const
    {
      return this->left == af.left && this->right == af.right
              && this->op == af.op && this->tag == af.tag;
    }

    /**
     * 重载小于号，stl_map中用
     * @param af
     * @return 
     */
    bool aalta_formula::operator< (const aalta_formula& af) const
    {
      if (this->left != af.left)
        return this->left < af.left;
      if (this->right != af.right)
        return this->right < af.right;
      if (this->op != af.op)
        return this->op < af.op;
      if (this->tag != af.tag)
        return this->tag < af.tag;
      return false;
    }

    /**
     * 生成tag信息
     * @param tag
     */
//    aalta_formula*
//    aalta_formula::classify (int tag)
//    {
//      aalta_formula ret = *this;
//      ret.tag = tag;
//      if (ret.left != NULL)
//        ret.left = ret.left->classify (tag);
//      if (ret.op == Until)
//        tag = (tag << 5) ^ (tag >> 27) ^ (size_t)this->unique ();
//      if (ret.right != NULL)
//        ret.right = ret.right->classify (tag);
//      ret.unique_prt = NULL;
//      ret.clc_hash ();
//      return ret.unique ();
//    }

    aalta_formula*
    aalta_formula::classify (af_list* tag_list)
    {
      bool null = false;
      if (tag_list == NULL)
        {
          null = true;
          tag_list = new af_list ();
        }
      af_list_set::iterator iter = all_list.find (tag_list);
      aalta_formula ret = *this;
      if (iter == all_list.end ())
        {
          ret.tag = new af_list (*tag_list);
          all_list.insert (ret.tag);
        }
      else
        ret.tag = *iter;
      if (ret.left != NULL)
        ret.left = ret.left->classify (tag_list);
      bool until = false;
      if (ret.op == Until)
        {
          until = true;
          tag_list->push_back (this->unique ());
        }
      if (ret.right != NULL)
        ret.right = ret.right->classify (tag_list);
      ret.unique_prt = NULL;
      ret.clc_hash ();
      if (until) tag_list->pop_back ();
      if (null) delete tag_list;
      return ret.unique ();
    }

    /**
     * 将非符号规范化，保证非只出现在原子之前
     * @param non
     * @return 
     */
    aalta_formula*
    aalta_formula::regulate (bool non)
    {
      if (non && this->op == Not)
        return this->right->regulate ();
      aalta_formula *l_af, *r_af, *afp;
      if (!non && this->op != Not)
        {
          l_af = left == NULL ? NULL : left->regulate ();
          r_af = right == NULL ? NULL : right->regulate ();
          return aalta_formula (this->op, l_af, r_af, tag).unique ();
        }
      if (non) afp = this;
      else afp = this->right;
      switch (afp->op)
        {
        case True:
          return aalta_formula::FALSE (tag);
        case False:
          return aalta_formula::TRUE (tag);
        case Not: //!!a = a
          return afp->right->regulate ();
        case Or: // !(a | b) = (!a) & (!b)
          l_af = afp->left->regulate (true);
          r_af = afp->right->regulate (true);
          return aalta_formula (And, l_af, r_af, tag).unique ();
        case And: // !(a & b) = (!a) | (!b)
          l_af = afp->left->regulate (true);
          r_af = afp->right->regulate (true);
          return aalta_formula (Or, l_af, r_af, tag).unique ();
        case Next: // !(X a) = X(! a)
          r_af = afp->right->regulate (true);
          return aalta_formula (Next, NULL, r_af, tag).unique ();
        case Until: // !(a U b) = (!a) R (!b)
          l_af = afp->left->regulate (true);
          r_af = afp->right->regulate (true);
          return aalta_formula (Release, l_af, r_af, tag).unique ();
        case Release: // !(a R b) = (!a) U (!b)
          l_af = afp->left->regulate (true);
          r_af = afp->right->regulate (true);
          return aalta_formula (Until, l_af, r_af, tag).unique ();
        default:
          return aalta_formula (Not, NULL, afp->unique (), tag).unique ();
        }
    }

    /**
     * 简化公式
     * @return 
     */
     /*
    aalta_formula*
    aalta_formula::simply ()
    {
      aalta_formula *l_s, *r_s;
      switch (this->op)
        {
        case And: // &
        case Or: // |
          {
            aalta_formula *af = this;
            std::string str;
            bool first = true;
            while (af != NULL)
              { // TODO: 有空优化之
                if (!first) str += " " + names[this->op] + " ";
                first = false;
                str += "(" + af->af_now (this->op)->simply ()->to_string () + ")";
                af = af->af_next (this->op);
              }
            aalta_formula tmp = aalta_formula (str);
            if (tmp.op == And)
              this->unique_prt = tmp.simply_and ();
            else if (tmp.op == Or)
              this->unique_prt = tmp.simply_or ();
            else
              this->unique_prt = tmp.unique ();
            break;
          }
        case Not: // !
          r_s = this->right->simply ();
          switch (r_s->op)
            {
            case True: // ! True = False
              this->unique_prt = aalta_formula::FALSE (this->tag);
              break;
            case False: // ! False = True
              this->unique_prt = aalta_formula::TRUE (this->tag);
              break;
            default:
              this->unique_prt = aalta_formula (Not, NULL, r_s, this->tag).unique ();
              break;
            }
          break;
        case Next: // X
          r_s = this->right->simply ();
          switch (r_s->op)
            {
            case True: // X True = True
              this->unique_prt = aalta_formula::TRUE (this->tag);
              break;
            case False: // X False = False
              this->unique_prt = aalta_formula::FALSE (this->tag);
              break;
            default:
              this->unique_prt = aalta_formula (Next, NULL, r_s, this->tag).unique ();
              break;
            }
          break;
        case Until: // U
          l_s = this->left->simply ();
          r_s = this->right->simply ();
          if (l_s->op == False // False U a = a
              || r_s->op == False // a U False = False
              || r_s->op == True // a U True = True
              || r_s->constituted_as (All, Or, l_s) // a U (a | ...) = a | ...
              || r_s->constituted_as (Left, Until, l_s) // a U (a U b) = a U b
              || r_s->constituted_as (Right, Until, l_s) // a U (b U a) = b U a
              || r_s->constituted_as (Right, Release, l_s) // a U (b R a) = b R a
              || l_s->constituted_as (Right, Release, r_s) // (b R a) U a = a;
              )
            this->unique_prt = r_s->unique ();
          else if (l_s->constituted_as (Left, Until, r_s)) // (a U b) U a = b U a
            this->unique_prt = aalta_formula (Until, l_s->right->unique (), l_s->left->unique (), this->tag).simply ();
          else if (l_s->constituted_as (Right, Until, r_s)) // (b U a) U a = b U a
            this->unique_prt = l_s->unique ();
          else if (l_s->constituted_as (Right, Next, r_s)) // X a U a = X a | a
            this->unique_prt = aalta_formula (Or, l_s->unique (), r_s->unique (), this->tag).simply ();
          else if (l_s->op == Next && r_s->op == Next) // X a U X b = X ( a U b )
            this->unique_prt = aalta_formula (Next,
                                              NULL,
                                              aalta_formula (Until, l_s->right, r_s->right, this->tag).unique (),
                                              this->tag).simply ();
          else
            this->unique_prt = aalta_formula (Until, l_s, r_s, this->tag).unique ();
          break;
        case Release: // R
          l_s = this->left->simply ();
          r_s = this->right->simply ();
          if (l_s->op == True // True R a = a
              || r_s->op == False // a R False = False
              || r_s->op == True // a R True = True
              || r_s->constituted_as (All, And, l_s) // a R (a & ...) = a & ...
              || l_s->constituted_as (All, Or, r_s) // (a | ...) R a = a
              || r_s->constituted_as (Left, Release, l_s) // a R (a R b) = a R b
              || r_s->constituted_as (Right, Release, l_s) // a R (b R a) = b R a
              || r_s->constituted_as (Right, Until, l_s) // a R (b U a) = b U a
              || l_s->constituted_as (All, Or, Right, Until, r_s) // (b U a | ... ) R a = a;
              )
            this->unique_prt = r_s->unique ();
          else if (l_s->constituted_as (Left, Release, r_s)) // (a R b) R a = b R a
            this->unique_prt = aalta_formula (Release, l_s->right->unique (), l_s->left->unique (), this->tag).simply ();
          else if (l_s->constituted_as (Right, Release, r_s)) // (b R a) R a = b R a
            this->unique_prt = l_s->unique ();
          else if (l_s->op == Next && r_s->op == Next) // X a R X b = X ( a R b )
            this->unique_prt = aalta_formula (Next,
                                              NULL,
                                              aalta_formula (Release, l_s->right, r_s->right, this->tag).unique (),
                                              this->tag).simply ();
          else if (l_s->op == Not && l_s->right->unique () == r_s->unique ()
                   || r_s->op == Not && r_s->right->unique () == l_s->unique ()) // !a R a = False R a
            this->unique_prt = aalta_formula (Release,
                                              aalta_formula::FALSE (this->tag),
                                              r_s,
                                              this->tag).simply ();
          else
            this->unique_prt = aalta_formula (Release, l_s, r_s, this->tag).unique ();
          //@ TODO: (b R (!a & ..) & ..) R a = False R a
          break;
        }
      *this = *this->unique ();
      return this->unique ();
    }
    */

    /**
     * 简化And公式
     * @return 
     */
    aalta_formula*
    aalta_formula::simply_and ()
    {
      if (aalta_formula::is_conflict (this->left, this->right))
        this->unique_prt = aalta_formula::FALSE (this->tag);
      else
        this->unique_prt = simply_and_weak ();
      *this = *this->unique_prt;
      return this->unique ();
    }

    /**
     * 简化And公式,不做冲突检测
     * @return 
     */
    aalta_formula*
    aalta_formula::simply_and_weak ()
    {
      af_id_set pos, neg;
      aalta_formula *l = left, *r = right;
      aalta_formula *l_n = l->af_now (And), *r_n = r->af_now (And);
      std::list<aalta_formula*> af_list;
      while (l != NULL && r != NULL)
        {
          if (l_n < r_n)
            {
              if (mutex (l_n, pos, neg))
                return this->unique_prt = aalta_formula::FALSE (this->tag);
              if (l_n->op != True)
                af_list.push_back (l_n);
              l = l->af_next (And);
              l_n = l == NULL ? NULL : l->af_now (And);
            }
          else if (l_n > r_n)
            {
              if (mutex (r_n, pos, neg))
                return this->unique_prt = aalta_formula::FALSE (this->tag);
              if (r_n->op != True)
                af_list.push_back (r_n);
              r = r->af_next (And);
              r_n = r == NULL ? NULL : r->af_now (And);
            }
          else
            {
              if (mutex (l_n, pos, neg))
                return this->unique_prt = aalta_formula::FALSE (this->tag);
              if (l_n->op != True)
                af_list.push_back (l_n);
              l = l->af_next (And);
              r = r->af_next (And);
              l_n = l == NULL ? NULL : l->af_now (And);
              r_n = r == NULL ? NULL : r->af_now (And);
            }
        }

      if (l != NULL && l->op != True)
        af_list.push_back (l);
      if (r != NULL && r->op != True)
        af_list.push_back (r);

      while (l != NULL)
        {
          if (mutex (l->af_now (And), pos, neg))
            return this->unique_prt = aalta_formula::FALSE (this->tag);
          l = l->af_next (And);
        }
      while (r != NULL)
        {
          if (mutex (r->af_now (And), pos, neg))
            return this->unique_prt = aalta_formula::FALSE (this->tag);
          r = r->af_next (And);
        }

      switch (af_list.size ())
        {
        case 0:
          this->unique_prt = aalta_formula::TRUE (this->tag);
          break;
        case 1:
          this->unique_prt = af_list.front ()->unique ();
          break;
        default:
          l = af_list.front ()->unique ();
          af_list.pop_front ();
          r = af_list.back ()->unique ();
          af_list.pop_back ();
          for (; !af_list.empty (); af_list.pop_back ())
            r = aalta_formula (And, af_list.back (), r, this->tag).unique ();
          this->unique_prt = aalta_formula (And, l, r, this->tag).unique ();
          break;
        }
      //      assert (check ());

      return this->unique_prt;
    }

    bool
    aalta_formula::check ()
    {
      aalta_formula* now = unique_prt->af_now (And);
      aalta_formula* next = unique_prt->af_next (And);
      while (next != NULL)
        {
          if (now->op == And || now >= next->af_now (And))
            return false;
          now = next->af_now (And);
          next = next->af_next (And);
        }
      return now->op != And;
    }

    /**
     * 简化Or公式
     * @return 
     */
    aalta_formula*
    aalta_formula::simply_or ()
    {
      af_id_set pos, neg;
      std::list<aalta_formula*> af_list;
      this->to_list (af_list);
      aalta_formula* pre = NULL;
      std::list<aalta_formula*>::iterator iter;
      for (iter = af_list.begin (); iter != af_list.end ();)
        {
          if (*iter == pre)
            { // a | a = a
              af_list.erase (iter++);
              continue;
            }
          pre = *iter;
          switch ((*iter)->op)
            {
            case False: // False | a... = a...
              af_list.erase (iter++);
              break;
            case True: // True | a... = True
              return this->unique_prt = aalta_formula::TRUE (this->tag);
            case Until:
              // a | b U (!a | .. ) = True
              // b U (a | .. ) | c U (!a | ..) = True;
              if ((*iter)->right->mutex (Or, pos, neg))
                return this->unique_prt = aalta_formula::TRUE (this->tag);
            default:
              // a | !a = True;
              if (aalta_formula::mutex (*iter, pos, neg))
                return this->unique_prt = aalta_formula::TRUE (this->tag);
              iter++;
              break;
            }
        }

      //a | b R a = a;
      //a | b U a = b U a;
      afp_set s1, s2;
      for (iter = af_list.begin (); iter != af_list.end (); iter++)
        {
          s1.insert (*iter);
          if ((*iter)->op == Until)
            s2.insert ((*iter)->right);
        }
      for (iter = af_list.begin (); iter != af_list.end ();)
        {
        /*
          if ((*iter)->op == Release && s1.find ((*iter)->right) != s1.end () // a | b R a = a
              || s2.find (*iter) != s2.end ()) // a | b U a = b U a
            af_list.erase (iter++);
          else
            iter++;
            */
        }
      return this->unique_prt = aalta_formula::list2af (Or, af_list, this->tag);
    }

    /**
     * 判断公式的构造形式, 判断制定位置的公式是否为af
     * @param pos 位置信息
     * @param op 当前公式的操作符
     * @param af
     * @return 
     */
    bool
    aalta_formula::constituted_as (poskind pos, int op, aalta_formula* af)
    {
      switch (pos)
        {
        case Left: // 公式操作符为op，且左边节点公式为af
          return this->op == op && this->left->unique () == af->unique ();
        case Right: // 公式操作符为op，且右边节点公式为af
          return this->op == op && this->right->unique () == af->unique ();
        case All: // 或和且操作符，判断里面是否存在一个af
          if (this->op != op) return this->unique () == af->unique ();
          return (this->left == NULL ? false : this->left->constituted_as (All, op, af)) ||
                  (this->right == NULL ? false : this->right->constituted_as (All, op, af));
        default:
          Printer::printErrorMessage ("Error: con't analyse this position! ");
          return false;
        }
    }

    /**
     * 同 constituted_as (poskind, int, aalta_formula*)
     * 再往下一层
     * @param pos1
     * @param op1
     * @param pos2
     * @param op2
     * @param af
     * @return 
     */
    bool
    aalta_formula::constituted_as (poskind pos1, int op1, poskind pos2, int op2, aalta_formula* af)
    {
      switch (pos1)
        {
        case Left:
          return this->op == op1 && this->left->constituted_as (pos2, op2, af);
        case Right:
          return this->op == op1 && this->right->constituted_as (pos2, op2, af);
        case All:
          if (this->op != op1) return this->constituted_as (pos2, op2, af);
          return (this->left == NULL ? false : this->left->constituted_as (All, op1, pos2, op2, af)) ||
                  (this->right == NULL ? false : this->right->constituted_as (All, op1, pos2, op2, af));
        default:
          Printer::printErrorMessage ("Error: con't analyse this position! ");
          return false;
        }
    }

    /**
     * 转化为list存储结构
     * @param af_list
     */
    void
    aalta_formula::to_list (std::list<aalta_formula*>& af_list)
    {
      switch (this->op)
        {
        case And:
        case Or:
          {
            aalta_formula *l = left, *r = right;
            aalta_formula *l_n = l->af_now (op), *r_n = r->af_now (op);
            while (l != NULL && r != NULL)
              {
                if (l_n < r_n)
                  {
                    af_list.push_back (l_n);
                    l = l->af_next (op);
                    l_n = l == NULL ? NULL : l->af_now (op);
                  }
                else if (l_n > r_n)
                  {
                    af_list.push_back (r_n);
                    r = r->af_next (op);
                    r_n = r == NULL ? NULL : r->af_now (op);
                  }
                else
                  {
                    af_list.push_back (l_n);
                    l = l->af_next (op);
                    r = r->af_next (op);
                    l_n = l == NULL ? NULL : l->af_now (op);
                    r_n = r == NULL ? NULL : r->af_now (op);
                  }
              }
            while (l != NULL)
              {
                af_list.push_back (l->af_now (this->op));
                l = l->af_next (this->op);
              }
            while (r != NULL)
              {
                af_list.push_back (r->af_now (this->op));
                r = r->af_next (this->op);
              }
            break;
          }
        default:
          af_list.push_back (this->unique ());
          break;
        }
    }

    /**
     * 判断当前公式的op子公式是否与集合中的元素互斥, 即是否存在 a 和 !a
     * @param op
     * @param pos
     * @param neg
     * @return 
     */
    bool
    aalta_formula::mutex (int op, af_id_set& pos, af_id_set& neg)
    {
      if (this->op == op)
        {
          aalta_formula *af = this;
          while (af != NULL)
            {
              if (aalta_formula::mutex (af->af_now (this->op), pos, neg))
                return true;
              af = af->af_next (this->op);
            }
          return false;

        }
      return aalta_formula::mutex (this, pos, neg);
    }

    /**
     * 介于aalta_formula中对于与或的存储，若为与或，返回左节点, 为当前节点
     * @param op
     * @return 
     */
    aalta_formula*
    aalta_formula::af_now (int op)
    {
      if (this->op == op)
        return this->left;
      return this->unique ();
    }

    /**
     * 介于aalta_formula中对于与或的存储，若为与或，返回右节点, 为下一个节点
     * @param op
     * @return 
     */
    aalta_formula*
    aalta_formula::af_next (int op)
    {
      if (this->op == op)
        return this->right;
      return NULL;
    }

    /**
     * 克隆出该对象的副本（需要在外部显式delete）
     * @return 
     */
    aalta_formula*
    aalta_formula::clone () const
    {
      return new aalta_formula (*this);
    }

    /**
     * 返回该aalta_formula对应的唯一指针
     * @return 
     */
    aalta_formula*
    aalta_formula::unique ()
    {
      if (this->unique_prt == NULL)
        {
          af_prt_map::const_iterator iter = all_afs.find (*this);
          if (iter != all_afs.end ())
            {
              this->unique_prt = iter->second;
            }
          else
            {
              this->unique_prt = this->clone ();
              all_afs[*this] = this->unique_prt;
            }
        }
      return this->unique_prt;
    }

    /**
     * 获取op操作符
     * @return 
     */
    int
    aalta_formula::oper () const
    {
      return this->op;
    }

    /**
     * 获取操作符op对应的名称
     * @return 
     */
    std::string
    aalta_formula::op_name () const
    {
      return names[this->op];
    }

    /**
     * 获取tag位置标签
     * @return 
     */
    aalta_formula::tag_t
    aalta_formula::get_tag () const
    {
      return this->tag;
    }

    /**
     * 获取left节点
     * @return 
     */
    aalta_formula*
    aalta_formula::l_af () const
    {
      return this->left;
    }

    /**
     * 获取right节点
     * @return 
     */
    aalta_formula*
    aalta_formula::r_af () const
    {
      return this->right;
    }

    /**
     * 获取公式长度
     * @return 
     */
    int
    aalta_formula::get_length ()
    {
      if (length <= 0)
        {
          this->length = 1;
          if (this->left != NULL) this->length += this->left->get_length ();
          if (this->right != NULL) this->length += this->right->get_length ();
        }
      return this->length;
    }

    /**
     * To String
     * @return 
     */
    std::string
    aalta_formula::to_string () const
    {

      if (left == NULL && right == NULL)
        return names[op];
      if (left == NULL)
        return "(" + names[op] + " " + right->to_string () + ")";
      if (right == NULL)
        return "(" + left->to_string () + " " + names[op] + ")";
      return "(" + left->to_string () + " " + names[op] + " " + right->to_string () + ")";

      /*
      //added by Jianwen Li
      if (left == NULL && right == NULL)
        return name;
      if (left == NULL)
        return "(" + name + " " + right->to_string () + ")";
      if (right == NULL)
        return "(" + left->to_string () + " " + name + ")";
      return "(" + left->to_string () + " " + name + " " + right->to_string () + ")";
      */
    }
      
    //added by Jianwen Li  
    std::string 
    aalta_formula::to_string(tool_t tool) const
    {
      std::string result = "";
      std::string NEXT, UNTIL, GLOBAL, FUTURE, NOT, FAS, TRU;
      if(tool == trppp)
      {
        NEXT = "next ";
        UNTIL = " until ";
        GLOBAL = "always ";
        FUTURE = "sometime "; 
        NOT = "not ";
        FAS = "FALSE";
        TRU = "TRUE";
      }  
      else
      {
        NEXT = "X ";
        UNTIL = " U ";
        GLOBAL = "G ";
        FUTURE = "F ";
        if(tool == pltl)
        {
          NOT = "~";
          FAS = "False";
          TRU = "True";
        }
        else
        {
          NOT = "!";
          FAS = "FALSE";
          TRU = "TRUE";
        }
      }
      
      if(left == NULL && right == NULL )
      {
        //ATOMS.insert(name);
        if(name.compare("FALSE") == 0)
        {
          return "(" + FAS + ")";
        }
        else
        if(name.compare("TRUE") == 0)
        {
          return "(" + TRU + ")";
        }
        
        return "(" + name + ")";
      }
      
      if(left == NULL)
      {
        if(name.compare("X") == 0)
        {
          result += "(" + NEXT + right->to_string(tool) + ")";
        }
        else
        if(name.compare("F") == 0)
        {
          result += "(" + FUTURE + right->to_string(tool) + ")";
        }
        else
        if(name.compare("G") == 0)
        {
          result += "(" + GLOBAL  + right->to_string(tool) + ")";
        }
        else
        if(name.compare("!") == 0)
        {
          result += "(" + NOT + right->to_string(tool) + ")";
        }
        
      }
      else
      if(right == NULL)
      {
        Printer::printErrorMessage("formula parser error: right child is NULL!\n");
        exit(0);
      }
      else
      {
        if(name.compare("U") == 0)
        {
          result += "(" + left->to_string(tool) + UNTIL + right->to_string(tool) + ")";
        }
        else
        if(name.compare("R") == 0)
        {
          switch(tool)
          {
            
            case smv:
            case smv_bmc:
            {
              result += "(" + left->to_string(tool) + "V" + right->to_string(tool) + ")";
              break;
            }
            case Aalta:
            {
              result = "(" + left->to_string(tool) + name + right->to_string(tool) + ")";
              break;
            }
            case trppp:
            {
              result += "(not " + left->to_string(tool) + 
                        ") until (" + "not " + right->to_string(tool) + ")";
              break;
            }          
            case pltl:
            {
              result += "(~ " + left->to_string(tool) + 
                        ") U (" + "~ " + right->to_string(tool) + ")";
              break;
            }
            default:
            {
              Printer::printErrorMessage("Cannot recognize the tool!\n");
              exit(0);
            }
          }
        }
        else
        if(name.compare("&") == 0 | name.compare("|") == 0)
        {
          result += "(" + left->to_string(tool) + name + right->to_string(tool) + ")";  
        }
        else
        if(name.compare("->") == 0)
        {
          result += "((" + NOT + left->to_string(tool) + ") | " + right->to_string(tool) + ")";
          
        }
        else
        if(name.compare("<->") == 0)
        {
          result += "(((" + NOT + left->to_string(tool) + ") | " + right->to_string(tool) + ") & ("
                              + left->to_string(tool) + " | (" + NOT + right->to_string(tool) + ")))";
        }
      }
     return result;
    }

  }

/*
//added by Jianwen Li
using namespace aalta;
    
int main(int argc, char** argv)
{
  std::string input;
  std::getline(std::cin, input);
  aalta_formula* af = new aalta_formula(input);
  std::cout << af->to_string() << std::endl;
  //std::cout << "fjdskf" << std::endl;
  return 0;
}
*/



