/* 
 * File:   aalta_formula.hh
 * Author: snowingsea
 *
 * AF = (AF | NULL) op (AF | NULL) + tag
 * 
 * Created on July 16, 2013, 9:58 AM
 */

#ifndef AALTA_FORMULA_HH
#define	AALTA_FORMULA_HH

#include <map>

#include <set>
#include <list>
#include <string>
#include <vector>
#include "utility.hh"
#include "public.hh"

#  include <tr1/unordered_map>
#  include <tr1/unordered_set>
   namespace Sgi = std::tr1;
#  define hash_map unordered_map
#  define hash_multimap unordered_multimap
#  define hash_set unordered_set
/*
#include <unordered_map>
#include <unordered_set>
namespace Sgi = std;
#define hash_map unordered_map
#define hash_multimap unordered_multimap
#define hash_set unordered_set

//#include "ltlast/allnodes.hh"
*/

#define OPNUM 14
//#define Sgi std

  namespace aalta
  {

    class aalta_formula
    {
    private:

      /* af公式的hash函数 */
      struct af_hash
      {

        size_t operator () (const aalta_formula& af) const
        {
          return af.hash;
        }
      };

/* 位置类型，判断公式组成结构时用 */
      enum poskind
      {
        Left, Right, All
      };

      typedef std::list<aalta_formula*> af_list;
      typedef af_list* tag_t;

      struct af_list_prt_hash
      {

        size_t operator () (const af_list* p_list)const
        {
          size_t hash = HASH_INIT;
          for (af_list::const_iterator it = p_list->begin (); it != p_list->end (); it++)
            {
              hash = (hash << 5) ^ (hash >> 27) ^ (size_t) (*it);
            }
          return hash;
        }
      };

      struct af_list_prt_eq
      {

        bool operator () (const af_list* list1, const af_list* list2)const
        {
          if (list1->size () != list2->size ()) return false;
          af_list::const_iterator it1 = list1->begin ();
          af_list::const_iterator it2 = list2->begin ();
          for (; it1 != list1->end (); it1++, it2++)
            {
              if (*it1 != *it2)
                return false;
            }
          return true;
        }
      };


    public:

      /* 操作符类型 */
      enum opkind
      {
        True,
        False,
        Literal,
        Not,
        Or,
        And,
        Next,
        Until,
        Release,
        Global,
        Future,
        Imply,
        Equiv,
        Undefined
      };

//      typedef spot::ltl::formula spot_formula;
      typedef Sgi::hash_map<aalta_formula, aalta_formula*, af_hash> af_prt_map;
      typedef Sgi::hash_set<aalta_formula*> afp_set;
      typedef Sgi::hash_set<int> af_id_set;
      typedef Sgi::hash_set<af_list*, af_list_prt_hash, af_list_prt_eq> af_list_set;
//      typedef std::map<aalta_formula, aalta_formula*> af_prt_map;
//      typedef std::set<aalta_formula*> afp_set;
//      typedef std::set<int> af_id_set;


      //////////////
      //静态成员变量//
      //////////////////////////////////////////////////
    private:
      static std::vector<std::string> names;            // 存储操作符的名称以及原子变量的名称
      static std::map<std::string, int> name_ids;       // 名称和对应的位置映射
      static af_prt_map all_afs;                        // 所有aalta_formula实体和对应唯一指针的映射
      static af_list_set all_list;
      //////////////////////////////////////////////////


      ///////////
      //成员变量//
      //////////////////////////////////////////////////
    private:
      tag_t tag;                  // 标签，相对于Until的位置信息
      int op;                   // 操作符
      aalta_formula* left;      // 操作符左端公式
      aalta_formula* right;     // 操作符右端公式
      aalta_formula* unique_prt;// 指向唯一指针标识
      size_t hash;              // hash值
      int length;               //公式长度
      //added by Jianwen Li
      std::string  name;        //operator string, or atom
      //////////////////////////////////////////////////

      //------------------------------------------------
      /* 成员方法 */
    public:
          
      aalta_formula ();
      aalta_formula (const aalta_formula& orig);
      aalta_formula (const std::string& input);
      aalta_formula (int op, aalta_formula* left, aalta_formula*right, tag_t tag = 0);

      aalta_formula& operator = (const aalta_formula& af);
      bool operator< (const aalta_formula& af)const;
      bool operator == (const aalta_formula& af)const;

      aalta_formula* simply ();
      aalta_formula* simply_and ();
      aalta_formula* simply_and_weak ();
      aalta_formula* regulate (bool non = false);
      aalta_formula* classify(af_list* tag_list = NULL);
      //aalta_formula* classify(int tag = 0);

      aalta_formula* unique ();
      aalta_formula* clone ()const;
      std::string to_string ()const;
      //added by Jianwen Li 
      std::string to_string (tool_t)const;

      int oper ()const;
      std::string op_name ()const;
      tag_t get_tag ()const;
      aalta_formula* l_af ()const;
      aalta_formula* r_af ()const;
      int get_length ();

      static aalta_formula* TRUE (tag_t tag = 0);
      static aalta_formula* FALSE (tag_t tag = 0);
      static void init_static ();
      static std::string get_name (int index);
      static void destroy ();
      static std::string all_to_string ();
      static std::string spot_formula_to_string (const std::string& input);
      
      //added by Jianwen Li
      std::string get_name() {return name;}
      void set_name(std::string s) { name = s;}
      void set_left(aalta_formula *af) {left = af;}
      void set_right(aalta_formula *af) {right = af;}

    private:
      //added by Jianwen Li
      aalta_formula* input_from(aalta_formula*);
      std::vector<aalta_formula*> get_elements(int);
      
      void init ();
      void clc_hash ();
      aalta_formula* af_now (int op);
      aalta_formula* af_next (int op);


      aalta_formula* simply_or ();
      bool mutex (int op, af_id_set& pos, af_id_set& neg);
      bool constituted_as (poskind pos, int op, aalta_formula* af);
      bool constituted_as (poskind pos1, int op1, poskind pos2, int op2, aalta_formula* af);
      void to_list (std::list<aalta_formula*>& af_list);
      
      bool check();

      static aalta_formula* list2af (int op, std::list<aalta_formula*>& af_list, tag_t tag = 0);
      static bool mutex (aalta_formula* af, af_id_set& pos, af_id_set& neg);
      static bool is_conflict (aalta_formula* af1, aalta_formula* af2);
    };
  }
#endif	/* AALTA_FORMULA_HH */

