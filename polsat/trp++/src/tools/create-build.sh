#!/bin/bash

FLAVORS=$(cat FLAVORS)

for flavor in $FLAVORS
do
  echo $flavor
  mkdir ./build/$flavor
  ./build/tools/symlinker.sh ./src ./build/$flavor $flavor
done

