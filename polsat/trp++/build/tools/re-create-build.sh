#!/bin/bash

FLAVORS=$(cat FLAVORS)

for flavor in $FLAVORS
do
  rm -rf build/$flavor
done

./build/tools/create-build.sh
