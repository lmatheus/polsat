#!/bin/bash

if [ "$3" == "" ]
then
  echo "Usage: symlinker.sh from to flavor"
  exit 1;
fi

FROM=$(build/tools/realpath $1)
TO=$(build/tools/realpath $2)
FLAVOR=$3

#echo $FROM
#echo $TO

for fulfrom in $FROM/*
do
file=$(basename $fulfrom)
#echo $file
#echo "$(basename $file _$FLAVOR)_$FLAVOR"
if [ "$(basename $file _$FLAVOR)_$FLAVOR" == "$file" ]
then
  file=$(basename $file _$FLAVOR)
  #echo $file
fi
fulto=$TO/$file
  if [ -d $fulfrom ]
  then
    if [ "$file" == "CVS" ]
    then
      continue
    fi

    if [ -e $fulto ]
    then 
      if [ ! -d $fulto  ] 
      then
        echo "Alert: $fulto is not a directory!"
        exit 2
      fi
    else
      mkdir $TO/$file
    fi
    ./build/tools/symlinker.sh $fulfrom $TO/$file $FLAVOR
  else
    if [ -e $fulto ]
    then
      if [ ! -f $fulto ]
      then
        echo "Alert: $fulto is not a regular file!"
	exit 3
      fi
    else
      ln -s $fulfrom $fulto
    fi
  fi
done
