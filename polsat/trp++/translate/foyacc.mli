type token =
  | TOK_EOF
  | TOK_IDENT of (string)
  | TOK_LPAR
  | TOK_RPAR
  | TOK_LBRACKET
  | TOK_RBRACKET
  | TOK_NOT
  | TOK_CONJ
  | TOK_DISJ
  | TOK_IMPL
  | TOK_EQUIV
  | TOK_COMMA
  | TOK_FORALL
  | TOK_EXISTS
  | TOK_ALWAYS
  | TOK_ALWAYSP
  | TOK_SOMETIME
  | TOK_SOMETIMEP
  | TOK_NEXT
  | TOK_UNTIL
  | TOK_TRUE
  | TOK_FALSE
  | TOK_UNLESS

val start :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Fotypes.formula
