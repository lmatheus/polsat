type token =
  | TOK_EOF
  | TOK_IDENT of (string)
  | TOK_LPAR
  | TOK_RPAR
  | TOK_LBRACKET
  | TOK_RBRACKET
  | TOK_NOT
  | TOK_CONJ
  | TOK_DISJ
  | TOK_IMPL
  | TOK_EQUIV
  | TOK_COMMA
  | TOK_FORALL
  | TOK_EXISTS
  | TOK_ALWAYS
  | TOK_ALWAYSP
  | TOK_SOMETIME
  | TOK_SOMETIMEP
  | TOK_NEXT
  | TOK_UNTIL
  | TOK_TRUE
  | TOK_FALSE
  | TOK_UNLESS

open Parsing;;
let _ = parse_error;;
# 2 "foyacc.mly"
open Fotypes;;

let rec introduceVars vlist form = match vlist with
  | head::tail -> introduceVars tail ( Fofunctions.subst form (Constant(head)) (Variable(head)) )
  | [] -> form
  ;;
  
(* checks whether a string represents a variable *)
let isVariable str = (Fofunctions.isUpper (str.[0]))
   ;;

# 41 "foyacc.ml"
let yytransl_const = [|
  257 (* TOK_EOF *);
  259 (* TOK_LPAR *);
  260 (* TOK_RPAR *);
  261 (* TOK_LBRACKET *);
  262 (* TOK_RBRACKET *);
  263 (* TOK_NOT *);
  264 (* TOK_CONJ *);
  265 (* TOK_DISJ *);
  266 (* TOK_IMPL *);
  267 (* TOK_EQUIV *);
  268 (* TOK_COMMA *);
  269 (* TOK_FORALL *);
  270 (* TOK_EXISTS *);
  271 (* TOK_ALWAYS *);
  272 (* TOK_ALWAYSP *);
  273 (* TOK_SOMETIME *);
  274 (* TOK_SOMETIMEP *);
  275 (* TOK_NEXT *);
  276 (* TOK_UNTIL *);
  277 (* TOK_TRUE *);
  278 (* TOK_FALSE *);
  279 (* TOK_UNLESS *);
    0|]

let yytransl_block = [|
  258 (* TOK_IDENT *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\002\000\003\000\003\000\004\000\004\000\
\007\000\008\000\008\000\005\000\005\000\005\000\005\000\005\000\
\005\000\005\000\005\000\005\000\005\000\005\000\005\000\005\000\
\005\000\005\000\006\000\006\000\009\000\000\000"

let yylen = "\002\000\
\002\000\001\000\001\000\001\000\001\000\004\000\003\000\003\000\
\003\000\001\000\003\000\001\000\001\000\002\000\003\000\003\000\
\003\000\003\000\002\000\002\000\002\000\002\000\002\000\003\000\
\003\000\003\000\001\000\003\000\001\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\012\000\013\000\030\000\000\000\
\002\000\003\000\004\000\000\000\000\000\014\000\000\000\000\000\
\000\000\019\000\000\000\021\000\000\000\023\000\001\000\000\000\
\000\000\000\000\000\000\000\000\000\000\029\000\000\000\000\000\
\026\000\000\000\000\000\007\000\008\000\015\000\000\000\000\000\
\000\000\000\000\000\000\006\000\000\000\000\000\009\000\028\000\
\011\000"

let yydgoto = "\002\000\
\015\000\016\000\017\000\018\000\019\000\039\000\024\000\043\000\
\040\000"

let yysindex = "\001\000\
\073\255\000\000\000\255\073\255\073\255\004\255\004\255\073\255\
\073\255\073\255\073\255\073\255\000\000\000\000\000\000\034\255\
\000\000\000\000\000\000\008\255\089\255\000\000\025\255\073\255\
\073\255\000\000\093\255\000\000\093\255\000\000\000\000\073\255\
\073\255\073\255\073\255\073\255\073\255\000\000\013\255\028\255\
\000\000\040\255\041\255\000\000\000\000\000\000\054\255\053\255\
\069\255\003\255\097\255\000\000\008\255\025\255\000\000\000\000\
\000\000"

let yyrindex = "\000\000\
\000\000\000\000\014\255\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\015\255\000\000\037\255\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\070\255\
\000\000\075\255\000\000\000\000\000\000\000\000\062\255\049\255\
\045\255\035\255\047\255\000\000\000\000\000\000\000\000\000\000\
\000\000"

let yygindex = "\000\000\
\000\000\252\255\000\000\000\000\000\000\030\000\077\000\042\000\
\000\000"

let yytablesize = 116
let yytable = "\021\000\
\022\000\001\000\020\000\026\000\027\000\028\000\029\000\030\000\
\023\000\038\000\032\000\033\000\034\000\035\000\005\000\020\000\
\052\000\005\000\020\000\044\000\045\000\005\000\005\000\005\000\
\005\000\037\000\042\000\046\000\047\000\048\000\049\000\050\000\
\051\000\005\000\031\000\024\000\005\000\022\000\024\000\053\000\
\022\000\032\000\033\000\034\000\035\000\018\000\055\000\025\000\
\018\000\017\000\025\000\054\000\017\000\036\000\024\000\018\000\
\037\000\017\000\017\000\017\000\032\000\032\000\016\000\034\000\
\018\000\016\000\025\000\018\000\017\000\025\000\016\000\017\000\
\016\000\027\000\003\000\004\000\032\000\033\000\034\000\005\000\
\010\000\016\000\056\000\025\000\016\000\006\000\007\000\008\000\
\009\000\010\000\011\000\012\000\041\000\013\000\014\000\057\000\
\032\000\033\000\034\000\035\000\032\000\033\000\034\000\035\000\
\032\000\033\000\034\000\035\000\036\000\000\000\000\000\037\000\
\036\000\000\000\000\000\037\000"

let yycheck = "\004\000\
\005\000\001\000\003\001\008\000\009\000\010\000\011\000\012\000\
\005\001\002\001\008\001\009\001\010\001\011\001\001\001\001\001\
\004\001\004\001\004\001\024\000\025\000\008\001\009\001\010\001\
\011\001\023\001\002\001\032\000\033\000\034\000\035\000\036\000\
\037\000\020\001\001\001\001\001\023\001\001\001\004\001\012\001\
\004\001\008\001\009\001\010\001\011\001\001\001\006\001\001\001\
\004\001\001\001\004\001\012\001\004\001\020\001\020\001\011\001\
\023\001\009\001\010\001\011\001\008\001\008\001\001\001\010\001\
\020\001\004\001\020\001\023\001\020\001\023\001\009\001\023\001\
\011\001\004\001\002\001\003\001\008\001\009\001\010\001\007\001\
\006\001\020\001\053\000\007\000\023\001\013\001\014\001\015\001\
\016\001\017\001\018\001\019\001\004\001\021\001\022\001\054\000\
\008\001\009\001\010\001\011\001\008\001\009\001\010\001\011\001\
\008\001\009\001\010\001\011\001\020\001\255\255\255\255\023\001\
\020\001\255\255\255\255\023\001"

let yynames_const = "\
  TOK_EOF\000\
  TOK_LPAR\000\
  TOK_RPAR\000\
  TOK_LBRACKET\000\
  TOK_RBRACKET\000\
  TOK_NOT\000\
  TOK_CONJ\000\
  TOK_DISJ\000\
  TOK_IMPL\000\
  TOK_EQUIV\000\
  TOK_COMMA\000\
  TOK_FORALL\000\
  TOK_EXISTS\000\
  TOK_ALWAYS\000\
  TOK_ALWAYSP\000\
  TOK_SOMETIME\000\
  TOK_SOMETIMEP\000\
  TOK_NEXT\000\
  TOK_UNTIL\000\
  TOK_TRUE\000\
  TOK_FALSE\000\
  TOK_UNLESS\000\
  "

let yynames_block = "\
  TOK_IDENT\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'formula) in
    Obj.repr(
# 55 "foyacc.mly"
           (
               _1;
           )
# 194 "foyacc.ml"
               : Fotypes.formula))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'atom) in
    Obj.repr(
# 59 "foyacc.mly"
                     (_1)
# 201 "foyacc.ml"
               : 'formula))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'quantified) in
    Obj.repr(
# 60 "foyacc.mly"
                     (_1)
# 208 "foyacc.ml"
               : 'formula))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'connective) in
    Obj.repr(
# 61 "foyacc.mly"
                     (_1)
# 215 "foyacc.ml"
               : 'formula))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 65 "foyacc.mly"
        (
	   Literal(Atom(_1,[]))
        )
# 224 "foyacc.ml"
               : 'atom))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'termlist) in
    Obj.repr(
# 69 "foyacc.mly"
        (
	    Literal(Atom(_1,_3))
        )
# 234 "foyacc.ml"
               : 'atom))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'boundvars) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 75 "foyacc.mly"
              (
	         let f = introduceVars _2 _3 in
	         Forall(_2, f)
              )
# 245 "foyacc.ml"
               : 'quantified))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'boundvars) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 80 "foyacc.mly"
              (
	         let f = introduceVars _2 _3 in
	         Exists(_2, f)
              )
# 256 "foyacc.ml"
               : 'quantified))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'varlist) in
    Obj.repr(
# 87 "foyacc.mly"
           (
               _2
           )
# 265 "foyacc.ml"
               : 'boundvars))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 92 "foyacc.mly"
           (
	       if not (isVariable _1)
	       then
	         (print_string (_1^ " is not uppercase\n"); 
		 raise Parsing.Parse_error; 
		 (* return any string *)
		 ["nonesense"])
	       else
	       [_1]
           )
# 281 "foyacc.ml"
               : 'varlist))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'varlist) in
    Obj.repr(
# 103 "foyacc.mly"
           (
	       _1::_3
           )
# 291 "foyacc.ml"
               : 'varlist))
; (fun __caml_parser_env ->
    Obj.repr(
# 109 "foyacc.mly"
              (
	         True
	      )
# 299 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    Obj.repr(
# 113 "foyacc.mly"
       (
	         False
	      )
# 307 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 117 "foyacc.mly"
              (
	          Not(_2)
              )
# 316 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formula) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 121 "foyacc.mly"
              (
	         And(_1,_3)
              )
# 326 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formula) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 125 "foyacc.mly"
              (
	         Or(_1,_3)
              )
# 336 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formula) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 129 "foyacc.mly"
              (
	         Implies(_1,_3)
              )
# 346 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formula) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 133 "foyacc.mly"
              (
	         And(Implies(_1,_3),Implies(_3,_1))
              )
# 356 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 137 "foyacc.mly"
       (
	         Always(_2)
	      )
# 365 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 141 "foyacc.mly"
       (
	         AlwaysP(_2)
	      )
# 374 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 145 "foyacc.mly"
       (
	         Sometime(_2)
	      )
# 383 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 149 "foyacc.mly"
       (
	         SometimeP(_2)
	      )
# 392 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 153 "foyacc.mly"
       (
	         Next(_2)
	      )
# 401 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formula) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 157 "foyacc.mly"
       (
	         Until(_1, _3)
	      )
# 411 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formula) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formula) in
    Obj.repr(
# 161 "foyacc.mly"
       (
	         Unless(_1, _3)
	      )
# 421 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'formula) in
    Obj.repr(
# 165 "foyacc.mly"
              (
                  _2;
              )
# 430 "foyacc.ml"
               : 'connective))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'term) in
    Obj.repr(
# 171 "foyacc.mly"
            (
	       [_1]
            )
# 439 "foyacc.ml"
               : 'termlist))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'term) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'termlist) in
    Obj.repr(
# 175 "foyacc.mly"
            (
	       _1::_3
            )
# 449 "foyacc.ml"
               : 'termlist))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 181 "foyacc.mly"
        (
	   Constant(_1)
        )
# 458 "foyacc.ml"
               : 'term))
(* Entry start *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let start (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Fotypes.formula)
